/*
 * JCADReportProcessor.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the code for processing received messages from a JCAD
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import gov.isa.model.AdminCode;
import gov.isa.model.Administrative;
import gov.isa.model.BoundedRange;
import gov.isa.model.CapabilityDeclaration;
import gov.isa.model.ChemicalReading;
import gov.isa.model.CommandDeclaration;
import gov.isa.model.CommandState;
import gov.isa.model.Config;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.DiscreteRange;
import gov.isa.model.Event;
import gov.isa.model.EventType;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.model.NameValuePair;
import gov.isa.model.ObservableDeclaration;
import gov.isa.model.ObservableState;
import gov.isa.model.ParameterDeclaration;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.PropertyState;
import gov.isa.model.Range;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ResultDeclaration;
import gov.isa.model.Secs;
import gov.isa.model.StandardIdentity;
import gov.isa.model.Status;
import gov.isa.model.Structure;
import gov.isa.model.Type;
import gov.isa.model.UCI;
import gov.isa.model.UTC;
import gov.isa.net.MetaData;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Extract information from a JCAD related message and write it to the output.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class JCADReportProcessor implements Runnable , IPCQueueListenerInterface
{

  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = Logger.getLogger( JCADReportProcessor.class );

  /**
   * The input queue.
   */
  private final IPCQueue<JCADDataQueueEntry> theQueue;

  /**
   * Flag indicating we should shut down.
   */
  private static final AtomicBoolean SHUTDOWN_FLAG = new AtomicBoolean( false );

  /**
   * The output file.
   */
  private final BufferedWriter outFile;

  /**
   * The display frame.
   */
  private final JHRMRecorderFrame theFrame;

  /**
   * The message information.
   */
  private Message theMsg;
  private Header theHdr;
  private MetaData theMeta;

  /**
   * The constructor
   *
   * @param outFile the logging output file
   */
  public JCADReportProcessor( BufferedWriter outFile )
  {
    theQueue = JHRMRecorder.getJCAD_QUEUE();
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    this.outFile = outFile;
    this.theFrame = JHRMRecorder.getMY_FRAME();
    MY_LOGGER.info( "Report processor constructed." );
  }

  /**
   * Set the shutdown flag
   *
   * @param val the value to be set
   */
  public void setShutdown( boolean val )
  {
    JCADReportProcessor.SHUTDOWN_FLAG.set( val );
  }

  /**
   * Processes the message, writes the record log entries, and returns the status
   *
   * @return flag indicating success/failure, ignoring unused messages is success
   */
  private boolean processMessage()
  {
    boolean retValue = false;
    KnownInfoList infoList = this.getKnownSensorInfoList();
    String theSensor = theMsg.getSource().uci();
    UTC reportTimeUTC = theMsg.getTime();
    double reportTime = reportTimeUTC.getValue().atomicValue();
    long identifier = theMsg.getIdentifier();
    if ( theMsg instanceof Administrative )
    {
      ArrayList<String> aboutUs = new ArrayList<>();
      ImmutableList<UCI> about = ( (Administrative) theMsg ).getAbout();
      about.stream().
        map( (x) -> x.uci() ).
        forEachOrdered( (work) ->
        {
          infoList.theList.stream().
            filter( (y) -> ( ( work.toUpperCase().contains( "JCAD" ) ) && ( y.enabled ) &&
                             ( !work.contains( "Replay" ) ) ) ).
            forEachOrdered( (_item) ->
            {
              aboutUs.add( work );
            } );
        } );
      theFrame.incrementAdminCount();
      retValue = dumpAdminMessage( theMsg , theSensor , identifier , reportTime , aboutUs );
    }
    else if ( theMsg instanceof Event )
    {
      theFrame.incrementEventCount();
      retValue = this.dumpEventMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Config )
    {
      theFrame.incrementConfigCount();
      retValue = this.dumpConfigMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Status )
    {
      theFrame.incrementStatusCount();
      retValue = this.dumpStatusMessage( theMsg , theSensor , identifier , reportTime );
    }
    else
    {
      retValue = true;  // Ignore things we don't care about
    }
    return ( retValue );
  }

  /**
   * Writes a received Admin message to the log.
   *
   * @param theMsg     the input Admin message
   * @param theSensor  the originating sensor
   * @param identifier the identifier
   * @param reportTime the report time
   * @param aboutUs    the list of known sensors
   *
   * @return flag indicating success/failure
   */
  private boolean dumpAdminMessage( Message theMsg , String theSensor , long identifier , double reportTime ,
                                    ArrayList<String> aboutUs )
  {
    boolean retValue;
    try
    {
      Administrative admin = (Administrative) theMsg;
      AdminCode code = admin.getCode();
      String codeStr = code.value();
      JSONObject finalObj = new JSONObject();
      JSONObject anObj = new JSONObject();
      //
      // Ignore types we don't process
      //
      if ( ( codeStr.contains( "BANDWIDTH" ) ) ||
           ( codeStr.contains( "FOLLOW" ) ) ||
           ( codeStr.contains( "SUBSCRIPT" ) ) ||
           ( codeStr.contains( "OTHER" ) ) )
      {
        return ( true );
      }

      try
      {
        anObj.put( "source" , theSensor );
        anObj.put( "identifier" , identifier );
        anObj.put( "time" , reportTime );
        if ( !aboutUs.isEmpty() )
        {
          anObj.put( "about" , aboutUs );
        }
        else
        {
          anObj.put( "about" , "[]" );
        }
        anObj.put( "code" , code.value() );
        finalObj.put( "Administrative" , anObj );
      }
      catch ( JSONException ex )
      {
        MY_LOGGER.error( "Exception building Admin message data." , ex );
        retValue = true;
        return ( retValue );
      }

      if ( code.value().equalsIgnoreCase( "DEVICE_ADDED" ) )
      {
        aboutUs.stream().
          map( (x) ->
          {
            updateDisplay( x + " has joined the network." );
            return x;
          } ).
          forEachOrdered( (_item) ->
          {
            theFrame.incrementSensorCount();
          } );
      }
      else if ( code.value().equals( "DEVICE_REMOVED" ) )
      {
        aboutUs.stream().
          map( (x) ->
          {
            updateDisplay( x + " has left the network." );
            return x;
          } ).
          forEachOrdered( (_item) ->
          {
            theFrame.decrementSensorCount();
          } );
      }
      else if ( code.value().equals( "DEVICE_NON_RESPONSIVE" ) )
      {
        aboutUs.forEach( (x) ->
        {
          updateDisplay( x + " is not responding on the network." );
        } );
      }
      else if ( code.value().equals( "DEVICE_REVIVED" ) )
      {
        aboutUs.forEach( (x) ->
        {
          updateDisplay( x + " is again responding on the network." );
        } );
      }
      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Admin message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Admin message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Admin message" , ex );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * A simple implementation to pretty-print JSON file.
   *
   * @param unformattedJsonString the string to be formatted
   *
   * @return the pretty version
   */
  private String prettyPrintJSON( String unformattedJsonString )
  {
    StringBuilder prettyJSONBuilder = new StringBuilder();
    int indentLevel = 0;
    boolean inQuote = false;
    for ( char charFromUnformattedJson : unformattedJsonString.toCharArray() )
    {
      switch ( charFromUnformattedJson )
      {
        case '"':
          // switch the quoting status
          inQuote = !inQuote;
          prettyJSONBuilder.append( charFromUnformattedJson );
          break;
        case ' ':
          // For space: ignore the space if it is not being quoted.
          if ( inQuote )
          {
            prettyJSONBuilder.append( charFromUnformattedJson );
          }
          break;
        case '{':
        case '[':
          // Starting a new block: increase the indent level
          prettyJSONBuilder.append( charFromUnformattedJson );
          if ( !inQuote )
          {
            indentLevel++;
          }
          appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          break;
        case '}':
        case ']':
          // Ending a new block; decrese the indent level
          if ( !inQuote )
          {
            indentLevel--;
          }
          appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          prettyJSONBuilder.append( charFromUnformattedJson );
          break;
        case ',':
          // Ending a json item; create a new line after
          prettyJSONBuilder.append( charFromUnformattedJson );
          if ( !inQuote )
          {
            appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          }
          break;
        default:
          prettyJSONBuilder.append( charFromUnformattedJson );
      }
    }
    return prettyJSONBuilder.toString();
  }

  /**
   * Print a new line with indention at the beginning of the new line.
   *
   * @param indentLevel   the current indentation level
   * @param stringBuilder the builder for the output string
   */
  private void appendIndentedNewLine( int indentLevel , StringBuilder stringBuilder )
  {
    stringBuilder.append( "\n" );
    for ( int i = 0; i < indentLevel; i++ )
    {
      // Assuming indention using 2 spaces
      stringBuilder.append( "  " );
    }
  }

  /**
   * Process and output a Status message
   *
   * @param theMsg     the received message
   * @param theSensor  the UCI of the sensor
   * @param identifier the identifier of the message
   * @param reportTime the time of the report
   *
   * @return success/failure
   */
  private boolean dumpStatusMessage( Message theMsg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Status message for: " + theSensor );
      this.updateDisplay( "Processing Status message for: " + theSensor );
      Status staMsg = (Status) theMsg;
      JSONObject finalObj = new JSONObject();
      JSONObject statusObj = new JSONObject();
      long priority = 80L;
      try
      {
        if ( staMsg.getPriority().isPresent() )
        {
          priority = staMsg.getPriority().get().atomicValue();
        }
        statusObj.put( "source" , theSensor );
        statusObj.put( "identifier" , identifier );
        statusObj.put( "priority" , priority );
        statusObj.put( "time" , reportTime );
        ImmutableList<NameValuePair> properties = staMsg.getProperties();
        if ( ( properties != null ) && ( !properties.isEmpty() ) )
        {
          JSONObject propertiesObj = new JSONObject();
          for ( NameValuePair base : properties )
          {
            if ( base.getName().equalsIgnoreCase( "AdditionalProperties" ) )
            {
              JSONObject addPropObj = new JSONObject();
              ImmutableList<NameValuePair> addProp = ( (CustomType) base.getValue() ).getFields();
              for ( NameValuePair y : addProp )
              {
                if ( y.getName().equalsIgnoreCase( "drawing_number" ) )
                {
                  addPropObj.put( "drawing_number" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "issue_number" ) )
                {
                  addPropObj.put( "issue_number" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "system_id" ) )
                {
                  addPropObj.put( "system_id" , Integer.parseInt( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "hardware_ident" ) )
                {
                  addPropObj.put( "hardware_ident" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "boot_loader" ) )
                {
                  addPropObj.put( "boot_loader" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "serial_number" ) )
                {
                  addPropObj.put( "serial_number" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "hw_version_string" ) )
                {
                  addPropObj.put( "hw_version_string" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "loader_software_drawing" ) )
                {
                  addPropObj.put( "loader_software_drawing" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "app_sw_drawing" ) )
                {
                  addPropObj.put( "app_sw_drawing" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "system_alert_status" ) )
                {
                  addPropObj.put( "system_alert_status" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "saturation_protection" ) )
                {
                  addPropObj.put( "saturation_protection" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "operating_mode" ) )
                {
                  addPropObj.put( "operating_mode" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "clock" ) )
                {
                  addPropObj.put( "clock" , (String) y.getValue() );
                }
                else if ( y.getName().equalsIgnoreCase( "sieve_life_remaining" ) )
                {
                  addPropObj.put( "sieve_life_remaining" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "atmospheric_pressure" ) )
                {
                  addPropObj.put( "atmospheric_pressure" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "sensor_temperature" ) )
                {
                  addPropObj.put( "sensor_temperature" , (String) y.getValue() );
                }
                else if ( y.getName().equalsIgnoreCase( "sieve_low_warning" ) )
                {
                  addPropObj.put( "sieve_low_warning" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "calibration_mode" ) )
                {
                  addPropObj.put( "calibration_mode" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "change_battery" ) )
                {
                  addPropObj.put( "change_battery" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "battery_low_warning" ) )
                {
                  addPropObj.put( "battery_low_warning" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "initial_health_check" ) )
                {
                  addPropObj.put( "initial_health_check" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "vibration_detected" ) )
                {
                  addPropObj.put( "vibration_detected" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "datalog_fault" ) )
                {
                  addPropObj.put( "datalog_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "detection_mode" ) )
                {
                  addPropObj.put( "detection_mode" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "clock_battery_fault" ) )
                {
                  addPropObj.put( "clock_battery_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "persistent_health_fault" ) )
                {
                  addPropObj.put( "persistent_health_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "eeprom_checksum_fault" ) )
                {
                  addPropObj.put( "eeprom_checksum_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "change_sieve_pack" ) )
                {
                  addPropObj.put( "change_sieve_pack" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "major_fault" ) )
                {
                  addPropObj.put( "major_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "change_battery_fault" ) )
                {
                  addPropObj.put( "change_battery_fault" , Boolean.parseBoolean( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "power_status" ) )
                {
                  addPropObj.put( "power_status" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "runtime_hrs" ) )
                {
                  addPropObj.put( "runtime_hrs" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "runtime_min" ) )
                {
                  addPropObj.put( "runtime_min" , Short.parseShort( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_1" ) )
                {
                  addPropObj.put( "message_1" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_2" ) )
                {
                  addPropObj.put( "message_2" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_3" ) )
                {
                  addPropObj.put( "message_3" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_4" ) )
                {
                  addPropObj.put( "message_4" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_5" ) )
                {
                  addPropObj.put( "message_5" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_6" ) )
                {
                  addPropObj.put( "message_6" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_7" ) )
                {
                  addPropObj.put( "message_7" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "message_8" ) )
                {
                  addPropObj.put( "message_8" , ( (String) y.getValue() ) );
                }
              }  // end loop
              propertiesObj.put( "AdditionalProperties" , addPropObj );
            }
            else if ( base.getName().equalsIgnoreCase( "Position" ) )
            {
              JSONObject posObj = new JSONObject();
              GeographicPosition pos = (GeographicPosition) ( base.getValue() );
              posObj.put( "latitude" , Double.toString( pos.getLatitude().degrees() ) );
              posObj.put( "longitude" , Double.toString( pos.getLongitude().degrees() ) );
              if ( pos.getAltitude().isPresent() )
              {
                posObj.put( "altitude" , Double.toString( pos.getAltitude().get().meters() ) );
              }
              propertiesObj.put( "Position" , posObj );
            }
            else if ( base.getName().equalsIgnoreCase( "Manufacturer" ) )
            {
              propertiesObj.put( "Manufacturer" , ( (String) base.getValue() ) );
            }
            else if ( base.getName().equalsIgnoreCase( "Identity" ) )
            {
              propertiesObj.put( "Identity" , ( (StandardIdentity) base.getValue() ).getSymbol().sidc() );
            }
            else if ( base.getName().equalsIgnoreCase( "Minimum Status Interval" ) )
            {
              propertiesObj.put( "Minimum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Maximum Status Interval" ) )
            {
              propertiesObj.put( "Maximum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Serial Number" ) )
            {
              propertiesObj.put( "Serial Number" , ( (String) base.getValue() ).trim() );
            }
            else if ( base.getName().equalsIgnoreCase( "Model" ) )
            {
              propertiesObj.put( "Model" , ( (String) base.getValue() ).trim() );
            }
            else
            {
              MY_LOGGER.debug( "Found property: " + base.getName() + " => " + base.getValue().toString() );
            }
          }
          statusObj.put( "properties" , propertiesObj );
        }
        retValue = true;
        finalObj.put( "Status" , statusObj );
      }
      catch ( NumberFormatException | JSONException ex )
      {
        MY_LOGGER.error( "Exception creating properties object." , ex );
        retValue = false;
      }
      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Status message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Status message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing received message" , ex );
      updateDisplay( "*** Exception processing received message" + ex.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * Logs the content of a configuration message for a sensor.
   *
   * @param msg        the Configuration message
   * @param theSensor  the sensor UCI
   * @param identifier the message ID
   * @param reportTime the message time
   *
   * @return success/failure
   */
  private boolean dumpConfigMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Config message for: " + theSensor );
      this.updateDisplay( "Processing Config message for: " + theSensor );
      Config config = (Config) msg;
      JSONObject finalObj = new JSONObject();
      try
      {
        JSONObject cfgObj = new JSONObject();
        cfgObj.put( "source" , theSensor );
        cfgObj.put( "identifier" , identifier );
        long priority = 80L;
        if ( config.getPriority().isPresent() )
        {
          priority = config.getPriority().get().atomicValue();
        }
        cfgObj.put( "priority" , priority );
        cfgObj.put( "time" , reportTime );

        if ( config.getCcd().isPresent() )
        {
          JSONObject ccdObj = new JSONObject();
          Optional<CapabilityDeclaration> ccd = config.getCcd();
          ImmutableList<PropertyDeclaration> properties = ccd.get().getProperties();
          if ( properties != null )
          {
            JSONArray propertiesObj = new JSONArray();
            for ( PropertyDeclaration x : properties )
            {
              JSONObject propDecl = new JSONObject();
              propDecl.put( "name" , x.getName() );
              propDecl.put( "description" , x.getDescription() );
              propDecl.put( "type" , ((Type)x.getType()).value() );
              propDecl.put( "mutability" , x.getMutability().value() );
              propDecl.put( "structure" , ((Structure)x.getStructure()) );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                propDecl.put( "range" , rangeDecl );
              }
              if ( x.getThreshold().isPresent() )
              {
                propDecl.put( "threshold" , x.getThreshold().toString() );
              }
              propertiesObj.put( propDecl );
            }
            ccdObj.put( "properties" , propertiesObj );
          }
          ImmutableList<CommandDeclaration> commands = ccd.get().getCommands();
          if ( commands != null )
          {
            JSONArray cmdObj = new JSONArray();
            for ( CommandDeclaration x : commands.asList() )
            {
              JSONObject cmdDecl = new JSONObject();
              cmdDecl.put( "name" , x.getName() );
              ImmutableList<ParameterDeclaration> args = x.getArgs();
              if ( ( args != null ) && ( !args.isEmpty() ) )
              {
                JSONArray params = new JSONArray();
                for ( ParameterDeclaration y : args.asList() )
                {
                  JSONObject paramDecl = new JSONObject();
                  paramDecl.put( "name" , y.getName() );
                  paramDecl.put( "description" , y.getDescription() );
                  paramDecl.put( "type" , ((Type)y.getType()).value() );
                  paramDecl.put( "optional" , y.getOptional() );
                  paramDecl.put( "structure" , ((Structure)y.getStructure()).toString() );
                  if ( y.getRange().isPresent() )
                  {
                    JSONObject rangeDecl = new JSONObject();
                    Optional<RangeDeclaration> rd = y.getRange();

                    JSONArray ranges = new JSONArray();
                    for ( Range r : rd.get().getRanges() )
                    {
                      JSONObject range = this.processRange( r );
                      ranges.put( range );
                    }
                    rangeDecl.put( "ranges" , ranges );
                  }
                  params.put( paramDecl );
                }
                cmdDecl.put( "args" , params );  // end args
              }
              if ( x.getDescription().isPresent() )
              {
                cmdDecl.put( "description" , x.getDescription().get() );
              }
              ImmutableList<ResultDeclaration> reslt = x.getResults();
              if ( ( reslt != null ) && ( !reslt.isEmpty() ) )
              {
                JSONArray results = new JSONArray();
                for ( ResultDeclaration z : reslt.asList() )
                {
                  JSONObject resltDecl = new JSONObject();
                  resltDecl.put( "name" , z.getName() );
                  resltDecl.put( "description" , z.getDescription() );
                  resltDecl.put( "type" , ((Type)z.getType()).value() );
                  resltDecl.put( "structure" , ((Structure)z.getStructure()).toString() );
                  resltDecl.put( "optional" , z.getOptional() );
                  if ( z.getRange().isPresent() )
                  {
                    Optional<Range> rd = z.getRange();

                    JSONObject range = this.processRange( (Range)rd );
                    resltDecl.put( "range" , range );
                  }
                  results.put( resltDecl );
                }
                cmdDecl.put( "Result Declaration" , results );  // results
              }
              cmdObj.put( cmdDecl );
            }
            ccdObj.put( "commands" , commands );  // commands
          }

          ImmutableList<ObservableDeclaration> observables = ccd.get().getObservables();
          if ( ( observables != null ) && ( !observables.isEmpty() ) )
          {
            JSONArray obsObj = new JSONArray();
            for ( ObservableDeclaration x : observables.asList() )
            {
              JSONObject obsDecl = new JSONObject();
              obsDecl.put( "name" , x.getName() );
              obsDecl.put( "description" , x.getDescription() );
              obsDecl.put( "type" , ((Type)x.getType()).value() );
              obsDecl.put( "structure" , ((Structure)x.getStructure()).toString() );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                obsDecl.put( "range" , rangeDecl );
              }
              obsObj.put( obsDecl );  // observables
            }
            ccdObj.put( "observables" , obsObj );
          }

          ImmutableList<CustomTypeDeclaration> types = ccd.get().getTypes();
          if ( ( types != null ) && ( !types.isEmpty() ) )
          {
            JSONArray typesObj = new JSONArray();
            for ( CustomTypeDeclaration x : types )
            {
              JSONArray custom = new JSONArray();
              
              JSONObject oneCustom = new JSONObject();
              oneCustom.put( "name" , x.getName() );
              oneCustom.put( "description" , x.getDescription() );
              if ( !x.getFields().isEmpty() )
              {
                JSONArray fieldsObj = new JSONArray();
                ImmutableList<FieldDeclaration> fields = x.getFields();
                for ( FieldDeclaration y : fields )
                {
                  JSONObject fldDecl = new JSONObject();
                  fldDecl.put( "name" , y.getName() );
                  fldDecl.put( "description" , y.getDescription() );
                  fldDecl.put( "type" , ((Type)y.getType()).value() );
                  fldDecl.put( "structure" , ((Structure)y.getStructure()).toString() );
                  fldDecl.put( "optional" , y.getOptional() );
                  fieldsObj.put( fldDecl );
                }
                oneCustom.put( "fields" , fieldsObj );
              }
              custom.put( oneCustom );
              typesObj.put( custom ); // custom type decl
            }
            ccdObj.put( "types" , typesObj ); // types
          }
          cfgObj.put( "ccd" , ccdObj );  // ccd
        }

        ImmutableList<PropertyState> propStates = config.getPropertyStates();
        if ( ( propStates != null ) && ( !propStates.isEmpty() ) )
        {
          JSONArray propStatesObj = new JSONArray();
          for ( PropertyState x : propStates )
          {
            JSONObject pState = new JSONObject();
            pState.put( "name" , x.getName() );
            pState.put( "ready" , x.getReady() );
            pState.put( "reporting" , x.getReporting() );
            propStatesObj.put( pState );  // prop state
          }
          cfgObj.put( "property states" , propStatesObj );  // property states
        }

        ImmutableList<ObservableState> observables = config.getObservableStates();
        if ( ( observables != null ) && ( !observables.isEmpty() ) )
        {
          JSONArray obsStateObj = new JSONArray();
          for ( ObservableState x : observables )
          {
            JSONObject obsState = new JSONObject();
            obsState.put( "name" , x.getName() );
            obsState.put( "ready" , x.getReady() );
            obsState.put( "reporting" , x.getReporting() );
            obsStateObj.put( obsStateObj );
          }
          cfgObj.put( "observable states" , obsStateObj );  // observables
        }

        ImmutableList<CommandState> cmds = config.getCommandStates();
        if ( ( cmds != null ) && ( !cmds.isEmpty() ) )
        {
          JSONArray cmdStateObj = new JSONArray();
          for ( CommandState x : cmds )
          {
            JSONObject cmdState = new JSONObject();
            cmdState.put( "name" , x.getName() );
            cmdState.put( "ready" , x.getReady() );
            cmdStateObj.put( cmdState );  // command state
          }
          cfgObj.put( "command states" , cmdStateObj );  // command states
        }

        ImmutableList<NameValuePair> extras = config.getExtras();
        if ( ( extras != null ) && ( !extras.isEmpty() ) )
        {
          JSONArray extrasObj = new JSONArray();
          for ( NameValuePair x : extras )
          {
            JSONObject nvPairObj = new JSONObject();
            nvPairObj.put( x.getName() , x.getValue().toString() );
            extrasObj.put( nvPairObj );
          }
          cfgObj.put( "extras" , extrasObj );  // extras
        }
        finalObj.put( "Config" , cfgObj );
        retValue = true;
      }
      catch ( IllegalStateException | JSONException exObj )
      {
        MY_LOGGER.error( "Exception building configuration." , exObj );
        retValue = false;
      }

      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Config message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Config message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Config message." , ex );
    }
    return ( retValue );
  }

  /**
   * Returns a JSON Range object or null if an exception occurs.
   * 
   * @param r the range to process
   * 
   * @return the JSON or null
   */
  private JSONObject processRange( Range r )
  {
    JSONObject retValue = new JSONObject();
    try
    {
      if ( r instanceof BoundedRange )
      {
        JSONObject bRange = new JSONObject();
        bRange.put( "high inclusive" , ( (BoundedRange) r ).getHighInclusive() );
        bRange.put( "low inclusive" , ( (BoundedRange) r ).getLowInclusive() );
        if ( r.getDescription().isPresent() )
        {
          bRange.put( "description" , r.getDescription().get() );
        }
        bRange.put( "high" , ( (BoundedRange) r ).getHigh().toString() );
        bRange.put( "low" , ( (BoundedRange) r ).getLow().toString() );
        retValue.put( "BoundedRange" , bRange );
      }
      else if ( r instanceof DiscreteRange )
      {
        JSONObject dRange = new JSONObject();
        if ( r.getDescription().isPresent() )
        {
          dRange.put( "description" , r.getDescription().get() );
        }
        if ( ( (DiscreteRange) r ).getInclusive().isPresent() )
        {
          dRange.put( "inclusive" , ( (DiscreteRange) r ).getInclusive().get().toString() );
        }
        dRange.put( "value" , ( (DiscreteRange) r ).getValue().toString() );
        retValue.put( "DiscreteRange" , dRange );
      }
    }
    catch ( JSONException ex )
    {
      MY_LOGGER.error( "Exception processing a Range" , ex );
      retValue = null;
    }
    return ( retValue );
  }

  /**
   * Process an event message.
   *
   * @param msg        the message
   * @param theSensor  the sensor UCI
   * @param identifier the message identifier
   * @param reportTime the time of the report
   *
   * @return the status of the result
   */
  private boolean dumpEventMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;

    try
    {
      Event event = (Event) msg;
      EventType.Predefined eventType = event.getType().predef();
      String logMsg = "Processing Event " + eventType.name() + " message from: " + theSensor;
      MY_LOGGER.info( logMsg );
      this.updateDisplay( logMsg );
      JSONObject finalObj = new JSONObject();
      JSONObject eventObj = new JSONObject();
      eventObj.put( "source" , theSensor );
      eventObj.put( "identifier" , identifier );
      long priority = 80L;
      if ( event.getPriority().isPresent() )
      {
        priority = event.getPriority().get().atomicValue();
      }
      eventObj.put( "priority" , priority );
      eventObj.put( "time" , reportTime );
      long eventId = event.getEventID().event_id();
      eventObj.put( "event ID" , eventId );
      if ( event.getEventReference().isPresent() )
      {
        JSONObject evtRefObj = new JSONObject();
        String refuci = event.getEventReference().get().getCreator().uci();
        long refevt = event.getEventReference().get().getId().event_id();
        double reftim = event.getEventReference().get().getTime().getValue().secs();
        evtRefObj.put( "creator" , refuci );
        evtRefObj.put( "id" , refevt );
        evtRefObj.put( "time" , reftim );
        eventObj.put( "event reference" , evtRefObj );
      }
      eventObj.put( "type" , eventType.name() );
      switch ( eventType )
      {
        case ALARM:
        case ALERT:
        case DETECTION:
        case MEASUREMENT:
        {
          ImmutableList<NameValuePair> observables = event.getObservables();
          if ( observables.size() > 0 )
          {
            JSONObject obsObj = new JSONObject();
            for ( NameValuePair x : observables )
            {
              if ( x.getName().equalsIgnoreCase( "Chemical Reading" ) )
              {
                JSONObject chemReading = new JSONObject();
                ChemicalReading cReading = (ChemicalReading) x.getValue();
                if ( cReading.getMaterialClass().isPresent() )
                {
                  chemReading.put( "material class" , cReading.getMaterialClass().get().value() );
                }
                else
                {
                  chemReading.put( "material class" , "UNKNOWN" );
                }
                if ( cReading.getMaterialName().isPresent() )
                {
                  chemReading.put( "material name" , cReading.getMaterialName().get() );
                }
                else
                {
                  chemReading.put( "material name" , "None" );
                }
                if ( cReading.getServiceNumber().isPresent() )
                {
                  chemReading.put( "service number" , cReading.getServiceNumber().get() );
                }
                else
                {
                  chemReading.put( "service number" , "None" );
                }
                if ( cReading.getHarmful().isPresent() )
                {
                  chemReading.put( "harmful" , cReading.getHarmful().get() );
                }
                else
                {
                  chemReading.put( "harmful" , true );
                }
                if ( cReading.getDensity().isPresent() )
                {
                  chemReading.put( "density" , cReading.getDensity().get().getValue().kgpm3() );
                }
                else
                {
                  chemReading.put( "density" , 0.0 );
                }
                if ( cReading.getGBarReading().isPresent() )
                {
                  chemReading.put( "g-bar reading" , cReading.getGBarReading().get().getValue().shortValue() );
                }
                else
                {
                  chemReading.put( "g-bar reading" , 0 );
                }
                if ( cReading.getHBarReading().isPresent() )
                {
                  chemReading.put( "h-bar reading" , cReading.getHBarReading().get().getValue().shortValue() );
                }
                else
                {
                  chemReading.put( "h-bar reading" , 0 );
                }
                obsObj.put( "Chemical Reading" , chemReading );
              }
              else if ( x.getName().equalsIgnoreCase( "Position" ) )
              {
                JSONObject posObj = new JSONObject();
                GeographicPosition pos = (GeographicPosition) ( x.getValue() );
                posObj.put( "latitude" , pos.getLatitude().degrees() );
                posObj.put( "longitude" , pos.getLatitude().degrees() );
                if ( pos.getAltitude().isPresent() )
                {
                  posObj.put( "altitude" , pos.getAltitude().get().meters() );
                }
                else
                {
                  posObj.put( "altitude" , 0.0 );
                }
                obsObj.put( "Position" , posObj );
              }
            }
            eventObj.put( "observables" , obsObj );  // end Observables
          }

          ImmutableList<NameValuePair> properties = event.getDetectorProperties();
          if ( !properties.isEmpty() )
          {
            JSONObject detPropObj = new JSONObject();
            for ( NameValuePair x : event.getDetectorProperties() )
            {
              if ( x.getName().equals( "AdditionalMeasurements" ) )
              {
                JSONObject addMeasObj = new JSONObject();
                ImmutableList<NameValuePair> detProp = ( (CustomType) x.getValue() ).getFields();
                if ( ( detProp != null ) && ( !detProp.isEmpty() ) )
                {
                  for ( NameValuePair p : detProp )
                  {
                    if ( p.getName().equalsIgnoreCase( "positive_fg_volts" ) )
                    {
                      addMeasObj.put( "positive_fg_volts" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "negative_fg_volts" ) )
                    {
                      addMeasObj.put( "negative_fg_volts" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "posMobCalibFac" ) )
                    {
                      addMeasObj.put( "posMobCalibFac" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "negMobCalibFac" ) )
                    {
                      addMeasObj.put( "negMobCalibFac" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_temperature" ) )
                    {
                      addMeasObj.put( "pos_temperature" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_temperature" ) )
                    {
                      addMeasObj.put( "neg_temperature" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_amp" ) )
                    {
                      addMeasObj.put( "pos_rip_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_amp" ) )
                    {
                      addMeasObj.put( "neg_rip_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_mob" ) )
                    {
                      addMeasObj.put( "pos_rip_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_mob" ) )
                    {
                      addMeasObj.put( "neg_rip_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_noise" ) )
                    {
                      addMeasObj.put( "pos_rip_noise" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_noise" ) )
                    {
                      addMeasObj.put( "neg_rip_noise" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_noise_g" ) )
                    {
                      addMeasObj.put( "pos_noise_g" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_noise_h" ) )
                    {
                      addMeasObj.put( "neg_noise_h" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_amp1" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_amp1" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_amp1" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_amp1" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_mob1" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_mob1" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_mob1" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_mob1" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_amp2" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_amp2" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_amp2" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_amp2" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_mob2" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_mob2" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_mob2" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_mob2" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_amp3" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_amp3" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_amp3" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_amp3" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_rip_chk_mob3" ) )
                    {
                      addMeasObj.put( "pos_rip_chk_mob3" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_rip_chk_mob3" ) )
                    {
                      addMeasObj.put( "neg_rip_chk_mob3" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak1_amp" ) )
                    {
                      addMeasObj.put( "pos_peak1_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak1_mob" ) )
                    {
                      addMeasObj.put( "pos_peak1_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak2_amp" ) )
                    {
                      addMeasObj.put( "pos_peak2_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak2_mob" ) )
                    {
                      addMeasObj.put( "pos_peak2_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak3_amp" ) )
                    {
                      addMeasObj.put( "pos_peak3_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "pos_peak3_mob" ) )
                    {
                      addMeasObj.put( "pos_peak3_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak1_amp" ) )
                    {
                      addMeasObj.put( "neg_peak1_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak1_mob" ) )
                    {
                      addMeasObj.put( "neg_peak1_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak2_amp" ) )
                    {
                      addMeasObj.put( "neg_peak2_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak2_mob" ) )
                    {
                      addMeasObj.put( "neg_peak2_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak3_amp" ) )
                    {
                      addMeasObj.put( "neg_peak3_amp" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "neg_peak3_mob" ) )
                    {
                      addMeasObj.put( "neg_peak3_mob" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "bars" ) )
                    {
                      addMeasObj.put( "bars" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "peak_bars" ) )
                    {
                      addMeasObj.put( "peak_bars" , ( Short.parseShort( (String) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "positive_spectrum" ) )
                    {
                      addMeasObj.put( "positive_spectrum" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "negative_spectrum" ) )
                    {
                      addMeasObj.put( "negative_spectrum" , (String) p.getValue() );
                    }
                  }
                  detPropObj.put( "AdditionalMeasurements" , addMeasObj );
                }
              }
              else
              {
                detPropObj.put( x.getName() , x.getValue().toString() );
              }
            }
            eventObj.put( "detector properties" , detPropObj );// end detector properties
          }
          finalObj.put( "Event" , eventObj );
          try
          {
            MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
            this.outFile.write( finalObj.toString() );
            this.outFile.newLine();
            theFrame.incrementLoggedCount();
            retValue = true;
          }
          catch ( IOException ioEx )
          {
            MY_LOGGER.error( "Exception writing Event message to the output file" , ioEx );
            this.updateDisplay( "Exception writing Event message to the output file: " + ioEx.getLocalizedMessage() );
            retValue = false;
          }
        }
      }
    }
    catch ( IllegalStateException | JSONException ovrEx )
    {
      MY_LOGGER.error( "Exception writing Event message to the output file" , ovrEx );
      this.updateDisplay( "Exception writing Event message to the output file: " + ovrEx.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * Process a queue event
   *
   * @param evt the queue event
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    JCADDataQueueEntry entry = this.theQueue.readFirst( 0L );
    if ( entry != null )
    {
      switch ( evt.getEventType() )
      {
        case IPC_SHUTDOWN:
          JCADReportProcessor.setSHUTDOWN_FLAG( true );
          break;

        case IPC_NEW:
          theMsg = entry.getTheMsg();
          theHdr = entry.getTheHdr();
          theMeta = entry.getTheMeta();
          if ( theMsg != null )
          {
            boolean retValue = this.processMessage();
            if ( retValue )
            {
              MY_LOGGER.info( "Successfully processed message: " + theMsg.getIdentifier().toString() );
            }
            else
            {
              MY_LOGGER.error( "Failed to process message: " + theMsg.getIdentifier().toString() );
            }
          }
          break;

        default:
          MY_LOGGER.info( "Received unhandled queue entry type " + evt.getEventType().name() );
          break;
      }
    }
  }

  /**
   * Thread execution.
   */
  @Override
  public void run()
  {
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    boolean doMore = true;
    try
    {
      while ( ( doMore ) && ( !JCADReportProcessor.getSHUTDOWN_FLAG() ) )
      {
        try
        {
          Thread.sleep( 500L );
        }
        catch ( InterruptedException intEx )
        {
          doMore = false;
          JCADReportProcessor.setSHUTDOWN_FLAG( true );
          MY_LOGGER.error( "Thread interrupted for JCAD" , intEx );
        }
      }
    }
    catch ( Exception ex )
    {
      MY_LOGGER.error( "Exception waiting for shutdown" , ex );
    }
  }

  /**
   * Write a line to the display area.
   *
   * @param msg the message to be written
   */
  private void updateDisplay( String msg )
  {
    theFrame.writeToViewingArea( msg );
  }

  /**
   * @return the SHUTDOWN_FLAG
   */
  public static boolean getSHUTDOWN_FLAG()
  {
    return ( SHUTDOWN_FLAG.get() );
  }

  /**
   * Set the value of the shutdown flag
   *
   * @param val the boolean value to set
   */
  public static void setSHUTDOWN_FLAG( boolean val )
  {
    JCADReportProcessor.SHUTDOWN_FLAG.set( val );
  }

  /**
   * Returns known sensor entry information for this application.
   *
   * @return KnownInfoList of the sensor data
   */
  private KnownInfoList getKnownSensorInfoList()
  {
    KnownInfoList retValue = new KnownInfoList();

    for ( int i = 0; i < ( JHRMRecorder.getKNOWN_TYPES().length ); i++ )
    {
      KnownInfo entry = new KnownInfo( JHRMRecorder.getKNOWN_TYPES()[ i ] , JHRMRecorder.getKNOWN_ENABLED()[ i ] );
      retValue.theList.add( entry );
    }
    return ( retValue );

  }

  /**
   * Sensor known information
   */
  class KnownInfo
  {

    /**
     * The sensor type name
     */
    String name;
    /**
     * Is the sensor enabled?
     */
    boolean enabled;

    /**
     * Constructor
     *
     * @param name    the sensor type name
     * @param enabled the sensor type enable flag
     */
    public KnownInfo( String name , Boolean enabled )
    {
      this.name = name;
      this.enabled = enabled;
    }
  }

  /**
   * A list of KnowInfo records
   */
  class KnownInfoList
  {

    /**
     * The list of entries
     */
    ArrayList<KnownInfo> theList = new ArrayList<>();

    /**
     * Constructor
     */
    public KnownInfoList()
    {
    }
  }
}
