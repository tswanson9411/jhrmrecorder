/*
 * JHRMRecorder.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the main program for the recorder.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.SwingUtilities;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Main class of the JHRM Recorder
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class JHRMRecorder implements Runnable
{

  /**
   * List of connected sensors
   */
  private final Set knownSensors = Collections.synchronizedSet( new HashSet( 50 ) );

  /**
   * Error/Debug Logger
   */
  private static Logger MY_LOGGER;

  /**
   * The properties file name
   */
  protected static final String PROPERTIES_FILE = "jhrm_recorder.properties";

  /**
   * The properties
   */
  private static final Properties CLIENT_PROPERTIES = new Properties();

  /**
   * Version String
   */
  private static final String PROPERTIES_VERSION_STRING = "V1.1";

  /**
   * Version Date String
   */
  private static final String PROPERTIES_VERSION_DATE = "17 Jul 2020";

  /**
   * Tool name
   */
  private static final String PROPERTIES_TOOL_NAME = "ISA Recorder for JHRM";

  /**
   * JHRM Recorder PKI
   */
  private static final String CLIENT_PKI = "JHRM-Recorder.jks";

  /**
   * JHRM Recorder PKI
   */
  private static final String CLIENT_PKI_PW1 = "JHRM-Recorder.jks.pw";

  /**
   * JHRM Recorder PKI
   */
  private static final String CLIENT_PKI_PW2 = "JHRM-Recorder.pw";

  /**
   * JHRM Output Directory
   */
  private static final String OUTPUT_DIR = "data";

  /**
   * The maximum number of pool threads allowed.
   */
  private static final int MAX_POOL_THREADS = 10;

  /**
   * The core pool thread count
   */
  private static final int CORE_POOL_THREADS = 5;

  /**
   * User base directory
   */
  private static String USER_BASE;

  /**
   * User base directory file
   */
  private static File BASE_DIR;

  /**
   * The JFrame for the GUI
   */
  private static final JHRMRecorderFrame MY_FRAME;

  /**
   * The known sensor types.
   */
  private static final String[] KNOWN_TYPES = KnownSensorType.knownTypeNames();

  /**
   * Known sensor enabled flags.
   */
  private static final Boolean[] KNOWN_ENABLED = KnownSensorType.knownTypeEnabled();

  /**
   * The blocking queue for running threads.
   */
  private static final BlockingQueue<Runnable> BLOCKING_QUEUE = new LinkedBlockingQueue( 30 );

  /**
   * The pool for executing threads, this uses a FixedThreadPool for execution.
   */
  private static ThreadPoolExecutor POOL;

  /**
   * Is this Windows or Linux
   */
  private static boolean isWindows = true;

  /**
   * Directory for PKI
   */
  private static File pkiDir = null;
  /**
   * Directory for logs
   */
  private static File logDir = null;
  /**
   * The properties file
   */
  private static File propFile = null;
  /**
   * The default output directory
   */
  private static File outDir = null;
  //
  // The processing IPC Queues
  //
  /**
   * The control queue
   */
  private static final IPCQueue<JHRMRecorderCtlQueueEntry> CTL_QUEUE = new IPCQueue( "Recorder_Ctl_Queue" );
  /**
   * The JCAD data queue
   */
  private static final IPCQueue<JCADDataQueueEntry> JCAD_QUEUE = new IPCQueue( "JCAD_Data_Queue" );
  /**
   * The Hapsite data queue
   */
  private static final IPCQueue<HapsiteDataQueueEntry> HAPSITE_QUEUE = new IPCQueue( "Hapsite_Data_Queue" );
  /**
   * The MultiRAE data queue
   */
  private static final IPCQueue<MultiRAEDataQueueEntry> MULTIRAE_QUEUE = new IPCQueue( "MultiRAE_Data_Queue" );
  /**
   * The message router data queue
   */
  private static final IPCQueue<RouterDataQueueEntry> ROUTER_QUEUE = new IPCQueue( "Router_Data_Queue" );

  static
  {
    //
    // Check to see if we need to create directories and set up files
    //
    try
    {
      String OS = System.getProperty( "os.name" ).toLowerCase();

      if ( !OS.contains( "win" ) )
      {
        isWindows = false;
      }

      try
      {
        if ( isWindows )
        {
          USER_BASE = System.getProperty( "user.home" ) + "\\JHRMRecorder";
        }
        else
        {
          USER_BASE = System.getProperty( "user.home" ) + "/JHRMRecorder";
        }
        BASE_DIR = new File( USER_BASE );
        if ( !BASE_DIR.exists() )
        {
          if ( getBASE_DIR().mkdir() )
          {
            logDir = new File( getBASE_DIR() , "logs" );

            if ( !( logDir.mkdir() ) )
            {
              System.err.println( "Fatal error creating " + logDir.getAbsolutePath() );
              System.exit( 3 );
            }
            pkiDir = new File( getBASE_DIR() , "keys" );

            if ( !( pkiDir.mkdir() ) )
            {
              System.err.println( "Fatal error creating " + pkiDir.getAbsolutePath() );
              System.exit( 3 );
            }

            outDir = new File( getBASE_DIR() , "data" );

            if ( !( outDir.mkdir() ) )
            {
              System.err.println( "Fatal error creating " + outDir.getAbsolutePath() );
              System.exit( 3 );
            }

            String[] kList = new String[]
            {
              getCLIENT_PKI() , getCLIENT_PKI_PW1() , getCLIENT_PKI_PW2()
            };

            try
            {
              for ( String x : kList )
              {
                File pkiFile = new File( pkiDir , x );
                BufferedInputStream inStr;
                BufferedOutputStream outStr;
                inStr = new BufferedInputStream( JHRMRecorder.class.getResourceAsStream(
                  "/com/domenix/jhrmrecorder/resources/keys/" + x ) );
                outStr = new BufferedOutputStream( new FileOutputStream( pkiFile ) );
                byte[] buf = new byte[ 2048 ];
                int rdLen;
                while ( ( rdLen = inStr.read( buf ) ) != -1 )
                {
                  outStr.write( buf , 0 , rdLen );
                }
                inStr.close();
                outStr.flush();
                outStr.close();
              }
            }
            catch ( IOException pkiEx )
            {
              MY_LOGGER.fatal( "Exception copying PKI" , pkiEx );
              System.exit( 4 );
            }
            String[] fList = new String[]
            {
              "JPEO-CBRND.jpg" , "JPEO_Med.jpg" , "JPEO_Lg.jpg" , "log4j.dtd" , "log4j2.xml" , "jhrm_recorder.properties"
            };

            for ( String x : fList )
            {
              BufferedInputStream str = null;
              BufferedOutputStream wrtr = null;
              try
              {
                str = new BufferedInputStream( JHRMRecorder.class.getResourceAsStream(
                  "/com/domenix/jhrmrecorder/resources/" + x ) );
                wrtr = new BufferedOutputStream( new FileOutputStream( new File( getBASE_DIR() , x ) ) );
                byte[] buf = new byte[ 2048 ];
                int rdLen;
                while ( ( rdLen = str.read( buf ) ) != -1 )
                {
                  wrtr.write( buf , 0 , rdLen );
                }
                str.close();
                wrtr.flush();
                wrtr.close();
              }
              catch ( IOException crEx )
              {
                System.err.println( "Fatal exception creating " + x + " = " + crEx.getLocalizedMessage() );
                try
                {
                  str.close();
                }
                catch ( IOException ioEx1 )
                {
                  System.err.println( "Exception closing " + x + " = " + ioEx1.getLocalizedMessage() );
                }
                try
                {
                  if ( wrtr != null )
                  {
                    wrtr.close();
                  }
                }
                catch ( IOException ioEx2 )
                {
                  System.err.println( "Exception closing " + x + " = " + ioEx2.getLocalizedMessage() );
                }
                System.exit( 1 );
              }
            }
          }
        }
      }
      catch ( RuntimeException baseEx )
      {
        System.err.println( "Fatal exception creating initial files: " + baseEx.getLocalizedMessage() );
        System.exit( 4 );
      }

      File log4j = new File( BASE_DIR , "log4j2.xml" );
      FileInputStream str = new FileInputStream( log4j );

      DOMConfigurator myConfig = new DOMConfigurator();
      myConfig.doConfigure( str , Logger.getRootLogger().getLoggerRepository() );

      Logger.getRootLogger().setLevel( Level.WARN );
      propFile = new File( BASE_DIR , PROPERTIES_FILE );

      getCLIENT_PROPERTIES().load( new FileInputStream( propFile ) );
      String level = getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.log.level" , "WARN" ).toLowerCase();
      Level aLevel = Level.INFO;
      if ( level.equalsIgnoreCase( "fatal" ) )
      {
        aLevel = Level.FATAL;
      }
      else if ( level.equalsIgnoreCase( "error" ) )
      {
        aLevel = Level.ERROR;
      }
      else if ( level.equalsIgnoreCase( "info" ) )
      {
        aLevel = Level.INFO;
      }
      else if ( level.equalsIgnoreCase( "debug" ) )
      {
        aLevel = Level.DEBUG;
      }
      Logger.getRootLogger().setLevel( aLevel );
      MY_LOGGER = Logger.getLogger( JHRMRecorder.class );
      if ( pkiDir != null )
      {
        CLIENT_PROPERTIES.setProperty( "jhrm.recorder.pki" , pkiDir.getAbsolutePath() );
        CLIENT_PROPERTIES.setProperty( "jhrm.recorder.logs" , logDir.getAbsolutePath() );
        CLIENT_PROPERTIES.setProperty( "jhrm.recorder.data" , outDir.getAbsolutePath() );
        CLIENT_PROPERTIES.store( new FileOutputStream( propFile ) , "Initial Setup Complete" );
      }
    }
    catch ( IOException ex )
    {
      System.err.print( "Exception initializing JHRM-Recorder:" + ex.getLocalizedMessage() );
      System.exit( 1 );
    }
    MY_FRAME = new JHRMRecorderFrame();
  }

  /**
   * Constructor
   */
  public JHRMRecorder()
  {
  }

  /**
   * Create the thread pool.
   *
   * @return successful / failed
   */
  public static boolean createThreadPool()
  {
    boolean retValue = false;
    try
    {
      POOL = new ThreadPoolExecutor( CORE_POOL_THREADS , MAX_POOL_THREADS , 5L , TimeUnit.SECONDS , BLOCKING_QUEUE );
      POOL.setKeepAliveTime( 10L , TimeUnit.SECONDS );
      retValue = true;
    }
    catch ( RuntimeException runEx )
    {
      MY_LOGGER.error( "Exception starting thread pool." , runEx );
    }
    return ( retValue );
  }

  /**
   * Create the thread pool.
   *
   * @return successful / failed
   */
  public static boolean destroyThreadPool()
  {
    boolean retValue = false;
    try
    {
      if ( ( POOL.isShutdown() ) || ( POOL.isTerminated() ) )
      {
        POOL = null;
        retValue = true;
      }
      else if ( POOL.isTerminating() )
      {
        MY_LOGGER.error( "Thread pool not shutting down after wait time." );
        POOL.shutdownNow();
        if ( !POOL.awaitTermination( 8L , TimeUnit.SECONDS ) )
        {
          MY_LOGGER.fatal( "******************** Thread pool not shutting down, exiting." );
          if ( ( JHRMRecorder.getMY_FRAME() != null ) && ( JHRMRecorder.getMY_FRAME().isShowing() ) )
          {
            MY_FRAME.writeToViewingArea( "******************** Thread pool not shutting down, exiting." );
            JHRMRecorder.getMY_FRAME().exitMenuItem.doClick();
          }
        }
        retValue = true;
      }
    }
    catch ( InterruptedException iEx )
    {
      MY_LOGGER.error( "Pool termination was interrupted." , iEx );
    }
    catch ( RuntimeException runEx )
    {
      MY_LOGGER.error( "Exception starting thread pool." , runEx );
    }
    return ( retValue );
  }

  /**
   * @param args the command line arguments
   */
  public static void main( String[] args )
  {
    try
    {
      //
      // Create the GUI and transfer control
      //
      SwingUtilities.invokeLater( new JHRMRecorder() );
    }
    catch ( Exception ex )
    {
      System.err.println( "Exception launching: " + ex.getLocalizedMessage() );
      System.exit( 2 );
    }
  }

  /**
   * Thread run
   */
  @Override
  public void run()
  {
    MY_LOGGER.warn( "JHRM Recorder Startup." );
    getMY_FRAME().addWindowListener( new CVT_Exit_Listener() );
    getMY_FRAME().setVisible( true );
  }

  /**
   * @return the MY_FRAME
   */
  public static JHRMRecorderFrame getMY_FRAME()
  {
    return MY_FRAME;
  }

//  /**
//   * @param aMY_FRAME the MY_FRAME to set
//   */
//  public static void setMY_FRAME( JHRMRecorderFrame aMY_FRAME )
//  {
//    MY_FRAME = aMY_FRAME;
//  }
  /**
   * @return the CLIENT_PROPERTIES
   */
  public static Properties getCLIENT_PROPERTIES()
  {
    return CLIENT_PROPERTIES;
  }

  /**
   * @return the PROPERTIES_VERSION_STRING
   */
  public static String getPROPERTIES_VERSION_STRING()
  {
    return PROPERTIES_VERSION_STRING;
  }

  /**
   * @return the PROPERTIES_TOOL_NAME
   */
  public static String getPROPERTIES_TOOL_NAME()
  {
    return PROPERTIES_TOOL_NAME;
  }

  /**
   * @return the isWindows
   */
  public static boolean isIsWindows()
  {
    return isWindows;
  }

  /**
   * Exit handler for the main frame.
   */
  static class CVT_Exit_Listener implements WindowListener
  {

    /**
     * Perform some cleanup when we exit.
     */
    private void willExit()
    {
      MY_LOGGER.setLevel( Level.INFO );
      MY_LOGGER.info( "JHRMRecorder is shutting down..." );
      FileWriter fw = null;

      if ( ( JHRMRecorder.getMY_FRAME() != null ) && ( JHRMRecorder.getMY_FRAME().isShowing() ) )
      {
        JHRMRecorder.getMY_FRAME().saveAppState();
        JHRMRecorder.getMY_FRAME().dispose();
      }

      try
      {
        File workFile = new File( BASE_DIR , PROPERTIES_FILE );
        fw = new FileWriter( workFile );

        JHRMRecorder.getCLIENT_PROPERTIES().store( fw , PROPERTIES_TOOL_NAME + "-" + getPROPERTIES_VERSION_STRING() );
        fw.close();
      }
      catch ( IOException ex )
      {
        MY_LOGGER.error( "Exception saving properties" , ex );
        if ( fw != null )
        {
          try
          {
            fw.close();
          }
          catch ( IOException ioEx )
          {
            MY_LOGGER.error( "Exception closing file writer" , ioEx );
          }
        }
      }
      MY_LOGGER.info( "JHRMRecorder shut down." );
      System.exit( 0 );
    }

    /**
     * Window opened event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowOpened( WindowEvent e )
    {
//    throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Window closing event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowClosing( WindowEvent e )
    {
      if ( MY_LOGGER != null )
      {
        MY_LOGGER.warn( "Main exiting..." );
      }

      willExit();
    }

    /**
     * Window close event handler.
     *
     * @param e the event
     */
    @Override
    public void windowClosed( WindowEvent e )
    {
      if ( MY_LOGGER != null )
      {
        MY_LOGGER.warn( "Main exiting..." );
      }

      willExit();
    }

    /**
     * Window iconified event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowIconified( WindowEvent e )
    {
//    throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Window de-iconified event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowDeiconified( WindowEvent e )
    {
//    throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Window activate event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowActivated( WindowEvent e )
    {
//    throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Window de-activate event receipt handler.
     *
     * @param e the event
     */
    @Override
    public void windowDeactivated( WindowEvent e )
    {
//    throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  /**
   * @return the USER_BASE
   */
  public static String getUSER_BASE()
  {
    return USER_BASE;
  }

  /**
   * @return the BASE_DIR
   */
  public static File getBASE_DIR()
  {
    return BASE_DIR;
  }

  /**
   * Is a sensor with a UCI of the provided value in the known sensors list.
   *
   * @param uci the sensor's UCI
   *
   * @return is known (true) or is not known (false)
   */
  public boolean isSensorKnown( String uci )
  {
    boolean retValue = false;

    if ( ( uci != null ) && ( !uci.trim().isEmpty() ) )
    {
      if ( getKnownSensors().contains( uci ) )
      {
        retValue = true;
      }
    }
    return ( retValue );
  }

  /**
   * Add a sensor to the known sensors list.
   *
   * @param uci the sensor's UCI
   *
   * @return success/failure
   */
  public boolean addSensor( String uci )
  {
    boolean retValue = false;

    if ( ( uci != null ) && ( !uci.trim().isEmpty() ) )
    {
      if ( !knownSensors.contains( uci ) )
      {
        retValue = true;
        getKnownSensors().add( uci );
        MY_FRAME.incrementSensorCount();
      }
    }
    return ( retValue );
  }

  /**
   * Add a sensor to the known sensors list.
   *
   * @param uci the sensor's UCI
   *
   * @return success/failure
   */
  public boolean removeSensor( String uci )
  {
    boolean retValue = false;

    if ( ( uci != null ) && ( !uci.trim().isEmpty() ) )
    {
      if ( getKnownSensors().contains( uci ) )
      {
        retValue = true;
        getKnownSensors().remove( uci );
        MY_FRAME.decrementSensorCount();
      }
    }
    return ( retValue );
  }

  /**
   * @return the CLIENT_PKI
   */
  public static String getCLIENT_PKI()
  {
    return CLIENT_PKI;
  }

  /**
   * @return the CLIENT_PKI_PW1
   */
  public static String getCLIENT_PKI_PW1()
  {
    return CLIENT_PKI_PW1;
  }

  /**
   * @return the CLIENT_PKI_PW2
   */
  public static String getCLIENT_PKI_PW2()
  {
    return CLIENT_PKI_PW2;
  }

  /**
   * @return the OUTPUT_DIR
   */
  public static String getOUTPUT_DIR()
  {
    return OUTPUT_DIR;
  }

  /**
   * @return the Message queue for JCAD messages
   */
  public static IPCQueue<JHRMRecorderCtlQueueEntry> getCTL_QUEUE()
  {
    return CTL_QUEUE;
  }

  /**
   * @return the knownSensors
   */
  public Set getKnownSensors()
  {
    return knownSensors;
  }

  /**
   * @return the KNOWN_TYPES
   */
  public static String[] getKNOWN_TYPES()
  {
    return KNOWN_TYPES;
  }

  /**
   * @return the POOL
   */
  public static ThreadPoolExecutor getPOOL()
  {
    return POOL;
  }

  /**
   * @return the KNOWN_ENABLED
   */
  public static Boolean[] getKNOWN_ENABLED()
  {
    return KNOWN_ENABLED;
  }

  /**
   * @return the JCAD_QUEUE
   */
  public static IPCQueue<JCADDataQueueEntry> getJCAD_QUEUE()
  {
    return JCAD_QUEUE;
  }

  /**
   * @return the HAPSITE_QUEUE
   */
  public static IPCQueue<HapsiteDataQueueEntry> getHAPSITE_QUEUE()
  {
    return HAPSITE_QUEUE;
  }

  /**
   * @return the MULTIRAE_QUEUE
   */
  public static IPCQueue<MultiRAEDataQueueEntry> getMULTIRAE_QUEUE()
  {
    return MULTIRAE_QUEUE;
  }

  /**
   * @return the ROUTER_QUEUE
   */
  public static IPCQueue<RouterDataQueueEntry> getROUTER_QUEUE()
  {
    return ROUTER_QUEUE;
  }

  /**
   * @return the PROPERTIES_VERSION_DATE
   */
  public static String getPROPERTIES_VERSION_DATE()
  {
    return PROPERTIES_VERSION_DATE;
  }

}
