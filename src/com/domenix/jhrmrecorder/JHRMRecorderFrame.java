/*
 * JHRMRecorderFrame.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the main program for the recorder.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueueEventType;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

/**
 * The main display of the JHRM Recorder
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class JHRMRecorderFrame extends javax.swing.JFrame
{

  /**
   * Icon for the application.
   */
  private final ImageIcon theIcon;

  /**
   * The Help About dialog
   */
  private JHRMAbout aboutBox;
  /**
   * The max number of lines available in a text area
   */
  private static final int MAX_LINES_IN_AREA = 2500;
  /**
   * The fudge factor to trigger when to erase lines
   */
  private static final int MAX_LINES_TRIGGER = 250;
  /**
   * The comparison value to determine if limiting is done.
   */
  private static final int MAX_LINES_COMPARE_VALUE = MAX_LINES_IN_AREA + MAX_LINES_TRIGGER;
  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = Logger.getLogger(JHRMRecorderFrame.class);

  /**
   * Message counters
   */
  private int adminMsgCnt = 0;
  private int configMsgCnt = 0;
  private int eventMsgCnt = 0;
  private int statusMsgCnt = 0;
  private int receivedMsgCnt = 0;
  private int logMsgCnt = 0;
  private int sensorCnt = 0;
  /**
   * File information
   */
  private boolean appendFlag = false;
  private File lastOutputDir = null;
  private File lastOutputFile = null;
  private File outJCAD = null;
  private File outHapsite = null;
  private File outMultiRAE = null;

  private BufferedWriter outFile;
  private BufferedWriter outFileJCAD;
  private BufferedWriter outFileHapsite;
  private BufferedWriter outFileMultiRAE;
  /**
   * The ISA interface
   */
  private JHRMRecorderISA isaInterface;
  private Future isaThread;
  /**
   * The MessageRouter interface
   */
  private MessageRouter routerInterface;
  private Future routerThread;
  /**
   * The JCAD report processor
   */
  private JCADReportProcessor jcadProcessor;
  private Future jcadThread;
  /**
   * The Hapsite report processor
   */
  private HapsiteReportProcessor hapsiteProcessor;
  private Future hapsiteThread;
  /**
   * The MultiRAE report processor
   */
  private MultiRAEReportProcessor multiraeProcessor;
  private Future multiraeThread;

  /**
   * Creates new form JHRMRecorderFrame
   */
  public JHRMRecorderFrame()
  {
    super();
    initComponents();
    MY_LOGGER.info("JHRMRecorder frame constructor.");
    String iName = System.getProperty("user.home").replace("\\" , "/") + "/JHRMRecorder/JPEO-CBRND.jpg";
    theIcon = new javax.swing.ImageIcon(iName);
    this.setIconImage(theIcon.getImage());
    int width = Integer.parseInt(JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.size.width"));
    int height = Integer.parseInt(JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.size.height"));
    int locX = Integer.parseInt(JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.loc.x"));
    int locY = Integer.parseInt(JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.loc.y"));
    String lastD = JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.lastout");
    if ( ( lastD != null ) && ( !lastD.isEmpty() ) )
    {
      lastOutputDir = new File(lastD);
    } else
    {
      lastOutputDir = new File(JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.data"));
    }
    String lastF = JHRMRecorder.getCLIENT_PROPERTIES().getProperty("jhrm.recorder.lastname");
    if ( ( lastF != null ) && ( !lastF.isEmpty() ) )
    {
      lastOutputFile = new File(lastF);
    } else
    {
      lastOutputFile = new File(lastOutputDir , "Capture.txt");
    }
    Dimension frameSize;
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    if ( width > 0 )
    {
      frameSize = new Dimension(width , height);
    } else
    {
      frameSize = this.getPreferredSize();
    }

    if ( frameSize.height <= 0 )
    {
      this.setSize(480 , 360);
      frameSize = this.getSize();
    }

    if ( frameSize.height > screenSize.height )
    {
      frameSize.height = screenSize.height - 5;
    }

    if ( frameSize.width > screenSize.width )
    {
      frameSize.width = screenSize.width - 5;
    }

    this.setSize(frameSize);
    this.setLocation(locX , locY);
  }

  /**
   * Increments the received message count and updates the display
   */
  public void incrementRecvdCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      receivedMsgCnt++;
      recvdCount.setText(Integer.toString(receivedMsgCnt));
    });
  }

  /**
   * Increments the logged message count and updates the display
   */
  public void incrementLoggedCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      logMsgCnt++;
      loggedCount.setText(Integer.toString(logMsgCnt));
    });
  }

  /**
   * Increments the received admin message count and updates the display
   */
  public void incrementAdminCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      adminMsgCnt++;
      adminCount.setText(Integer.toString(adminMsgCnt));
    });
  }

  /**
   * Increments the received config message count and updates the display
   */
  public void incrementConfigCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      configMsgCnt++;
      cfgCount.setText(Integer.toString(configMsgCnt));
    });
  }

  /**
   * Increments the received status message count and updates the display
   */
  public void incrementStatusCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      statusMsgCnt++;
      statusCount.setText(Integer.toString(statusMsgCnt));
    });
  }

  /**
   * Increments the received event message count and updates the display
   */
  public void incrementEventCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      eventMsgCnt++;
      eventCount.setText(Integer.toString(eventMsgCnt));
    });
  }

  /**
   * Increments the connected sensor count and updates the display
   */
  public void incrementSensorCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      sensorCnt++;
      sensorCount.setText(Integer.toString(sensorCnt));
    });
  }

  /**
   * Decrements the connected sensor count and updates the display
   */
  public void decrementSensorCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      sensorCnt--;
      sensorCount.setText(Integer.toString(sensorCnt));
    });
  }

  /**
   * Clears the received message count and updates the display
   */
  public void clearRecvdCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      receivedMsgCnt = 0;
      recvdCount.setText(Integer.toString(receivedMsgCnt));
    });
  }

  /**
   * Clears the logged message count and updates the display
   */
  public void clearLoggedCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      logMsgCnt = 0;
      loggedCount.setText(Integer.toString(logMsgCnt));
    });
  }

  /**
   * Set the connection activity value.
   *
   * @param val the value to be set
   */
  public void setConnActValue(final String val)
  {
    SwingUtilities.invokeLater(() ->
    {
      connActValue.setText(val);
    });
  }

  /**
   * Clears all display counts.
   */
  public void clearAllCounts()
  {
    SwingUtilities.invokeLater(() ->
    {
      adminMsgCnt = 0;
      configMsgCnt = 0;
      eventMsgCnt = 0;
      logMsgCnt = 0;
      receivedMsgCnt = 0;
      sensorCnt = 0;
      adminCount.setText("0");
      cfgCount.setText("0");
      eventCount.setText("0");
      loggedCount.setText("0");
      recvdCount.setText("0");
      sensorCount.setText("0");
      statusCount.setText("0");
    });
  }

  /**
   * Clears the connected sensor count and updates the display
   */
  public void clearSensorCount()
  {
    SwingUtilities.invokeLater(() ->
    {
      sensorCnt = 0;
      sensorCount.setText(Integer.toString(sensorCnt));
    });
  }

  /**
   * Writes the provided message to the status line
   *
   * @param msg the message to be written
   */
  public void writeToStatus(String msg)
  {
    SwingUtilities.invokeLater(() ->
    {
      mainStatusText.setText(msg);
    });
  }

  /**
   * Show the context menu (right mouse menu)
   *
   * @param ta   the text area
   * @param xLoc the x location of the menu
   * @param yLoc the y location of the menu
   */
  private void showRMM(final JTextArea ta , final int xLoc , final int yLoc)
  {
    SwingUtilities.invokeLater(() ->
    {
      SearchPopupMenu popup = new SearchPopupMenu(ta);
      popup.show(ta , xLoc , yLoc);
    });
  }

  /**
   * Write information to the viewing area of the main form, a line feed will be automatically appended.
   *
   * @param msg the text to be written
   */
  public void writeToViewingArea(final String msg)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        try
        {
          MY_LOGGER.info(msg);
          logTextArea.append(msg);
          logTextArea.append("\n");
          limitTextArea(logTextArea);
          scrollToEndOfArea(logTextArea);
        } catch ( Exception ex )
        {
          MY_LOGGER.error("Exception writing to viewing data area" , ex);
        }
      }
    });
  }

//  /**
//   * Write a line of data to the output log and display.
//   * 
//   * @param line 
//   */
//  private void writeToDataLog( String line )
//  {
//    if ( outFile != null )
//    {
//      writeToOutput( line );
//      writeToViewingArea( line );
//      incrementLoggedCount();
//    }
//    else
//    {
//      MY_LOGGER.error( "Error: output file is not open." );
//    }
//  }
  /**
   * Writes a line to the output message capture file. If the parameter is null or an empty string it just writes a
   * newline.
   *
   * @param line the text to be written
   * @param which to which output file
   */
  private void writeToOutput( String line , BufferedWriter which )
  {
    try
    {
      if ( ( line != null ) && ( !line.isEmpty() ) )
      {
        which.write(line);
      }
      which.newLine();
    } catch ( IOException ex )
    {
      MY_LOGGER.error("Exception writing line to output file" , ex);
    }
  }

  /**
   * Limit the size of the buffer feeding the send and receive text buffers by removing lines from the top.
   *
   * @param ta JTextArea to be limited
   */
  private void limitTextArea(JTextArea ta)
  {
    try
    {
      int taCount = ta.getLineCount();
      if ( taCount >= MAX_LINES_COMPARE_VALUE )
      {
        int upTo = ta.getLineStartOffset(MAX_LINES_TRIGGER);
        ta.replaceRange(null , 0 , upTo);
      }
    } catch ( BadLocationException ex )
    {
      MY_LOGGER.error("Exception limiting lines in text area" , ex);
    }
  }

  /**
   * Scroll to the top of a text area
   *
   * @param a the text area
   */
  public void topOfViewingArea(final JTextArea a)
  {
    SwingUtilities.invokeLater(() ->
    {
      try
      {
        a.setCaretPosition(0);
      } catch ( Exception ex )
      {
        MY_LOGGER.error("Exception going to the top of viewing data area" , ex);
      }
    });
  }

  /**
   * Scroll to the end of a text area.
   *
   * @param a the text area
   */
  void scrollToEndOfArea(JTextArea a)
  {
    try
    {
      int cnt = a.getLineCount();
      int lineEnd = a.getLineEndOffset(cnt - 1);

      a.setCaretPosition(lineEnd);
    } catch ( BadLocationException ex )
    {
      MY_LOGGER.error("Exception scrolling to end of text area data" , ex);
    }
  }

  /**
   * Clears the viewing area
   */
  public void clearViewingArea()
  {
    logTextArea.setText("");
  }

  /**
   * Clear the status line
   */
  public void clearStatus()
  {
    SwingUtilities.invokeLater(() ->
    {
      mainStatusText.setText("");
    });
  }

  /**
   * Save the application's current state.
   */
  public void saveAppState()
  {
    Dimension mySize = JHRMRecorder.getMY_FRAME().getSize();
    int myWidth = (int) mySize.getWidth();
    int myHeight = (int) mySize.getHeight();
    Point loc = JHRMRecorder.getMY_FRAME().getLocation();
    int locX = loc.x;
    int locY = loc.y;
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.loc.x" , Integer.toString(locX));
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.loc.y" , Integer.toString(locY));
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.size.width" , Integer.toString(myWidth));
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.size.height" , Integer.toString(myHeight));
    if ( lastOutputDir != null )
    {
      JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.lastout" , lastOutputDir.getAbsolutePath());
    } else
    {
      JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.lastout" ,
        JHRMRecorder.getUSER_BASE() + File.separator + "data");
    }
    if ( lastOutputFile != null )
    {
      JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.lastname" , lastOutputFile.getAbsolutePath());
    } else
    {
      JHRMRecorder.getCLIENT_PROPERTIES().setProperty("jhrm.recorder.lastname" ,
        JHRMRecorder.getUSER_BASE() + File.separator + "data"
        + File.separator + "Capture.capout");
    }
  }

  /**
   * Utility to select a file using a specified file filter, dialog title, and an initial directory.
   *
   * @param filter  FileFilter for selecting files
   * @param title   String containing the dialog title
   * @param sel     The prior file that was selected
   * @param dir     File indicating the initial directory for selection
   * @param dirOnly is this a directory without a file
   * @param bothDF  this has both a directory and a file
   *
   * @return File that was selected or null if canceled
   */
  private File selectFile(javax.swing.filechooser.FileFilter filter , String title , File dir , File sel ,
    boolean dirOnly , boolean bothDF)
  {
    JFileChooser fc = new JFileChooser();
    int state;
    File which = null;

    if ( dirOnly )
    {
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    } else if ( bothDF )
    {
      fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    }

    if ( dir != null )
    {
      fc.setCurrentDirectory(dir);
    }

    if ( sel != null )
    {
      fc.setSelectedFile(sel);
    }

    if ( filter != null )
    {
      fc.setFileFilter(filter);
      fc.setAcceptAllFileFilterUsed(false);
    }

    fc.setDialogTitle(title);
    state = fc.showOpenDialog(JHRMRecorder.getMY_FRAME());

    if ( state == JFileChooser.APPROVE_OPTION )
    {
      which = fc.getSelectedFile();
      String fName = which.getAbsolutePath();
      if ( FilenameUtils.getExtension(fName).isEmpty() )
      {
        fName = fName + ".txt";
        which = new File(fName);
      }
      if ( which.exists() )
      {
        // Ask if the user wants to overwrite.
        int overWrite = JOptionPane.showConfirmDialog(
          JHRMRecorder.getMY_FRAME() ,
          "The file already exists.\nIf you want to overwrite this file select Yes\nTo append select No" ,
          "Overwrite or Append" , JOptionPane.YES_NO_OPTION , JOptionPane.QUESTION_MESSAGE);
        switch ( overWrite )
        {
          case JOptionPane.YES_OPTION:
            appendFlag = false;
            break;
          case JOptionPane.NO_OPTION:
            appendFlag = true;
            break;
          default:
            appendFlag = false;
            which = null;
            break;
        }
      }
    }

    return ( which );
  }

  /**
   * Pop up the About box.
   */
  public void showAboutBox()
  {
    if ( aboutBox == null )
    {
      aboutBox = new JHRMAbout(JHRMRecorder.getMY_FRAME() , false);
      aboutBox.setLocationRelativeTo(JHRMRecorder.getMY_FRAME());
    }

    aboutBox.setVisible(true);
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
   * content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents()
  {

    mainPanel = new javax.swing.JPanel();
    dataPanel = new javax.swing.JPanel();
    textScrollPane = new javax.swing.JScrollPane();
    logTextArea = new javax.swing.JTextArea();
    countPanel = new javax.swing.JPanel();
    recvdLabel = new javax.swing.JLabel();
    recvdCount = new javax.swing.JTextField();
    loggedLabel = new javax.swing.JLabel();
    loggedCount = new javax.swing.JTextField();
    adminLabel = new javax.swing.JLabel();
    adminCount = new javax.swing.JTextField();
    cfgLabel = new javax.swing.JLabel();
    cfgCount = new javax.swing.JTextField();
    statusLabel = new javax.swing.JLabel();
    statusCount = new javax.swing.JTextField();
    eventLabel = new javax.swing.JLabel();
    eventCount = new javax.swing.JTextField();
    sensorCntLabel = new javax.swing.JLabel();
    sensorCount = new javax.swing.JTextField();
    editPanel = new javax.swing.JPanel();
    fileLabel = new javax.swing.JLabel();
    selectFileButton = new javax.swing.JButton();
    fileText = new javax.swing.JTextField();
    wrapCheckBox = new javax.swing.JCheckBox();
    connBtn = new javax.swing.JButton();
    connStatusLabel = new javax.swing.JLabel();
    connActValue = new javax.swing.JTextField();
    mainStatusPanel = new javax.swing.JPanel();
    mainStatusLabel = new javax.swing.JLabel();
    mainStatusText = new javax.swing.JTextField();
    recorderMenuBar = new javax.swing.JMenuBar();
    fileMenu = new javax.swing.JMenu();
    exitMenuItem = new javax.swing.JMenuItem();
    editMenu = new javax.swing.JMenu();
    editMenuItem = new javax.swing.JMenuItem();
    editConnMenuItem = new javax.swing.JMenuItem();
    editSelectMenuItem = new javax.swing.JMenuItem();
    helpMenuItem = new javax.swing.JMenu();
    aboutBoxMenuItem = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("JHRM ISA Recorder");

    mainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Logged Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
    mainPanel.setLayout(new java.awt.BorderLayout());

    dataPanel.setLayout(new java.awt.BorderLayout());

    logTextArea.setEditable(false);
    logTextArea.setColumns(20);
    logTextArea.setLineWrap(true);
    logTextArea.setRows(40);
    logTextArea.setTabSize(2);
    logTextArea.setWrapStyleWord(true);
    logTextArea.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        logTextAreaMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        logTextAreaMouseReleased(evt);
      }
    });
    textScrollPane.setViewportView(logTextArea);

    dataPanel.add(textScrollPane, java.awt.BorderLayout.CENTER);

    countPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    recvdLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    recvdLabel.setText("Recvd: ");
    countPanel.add(recvdLabel);

    recvdCount.setEditable(false);
    recvdCount.setColumns(5);
    recvdCount.setText("00000");
    countPanel.add(recvdCount);

    loggedLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    loggedLabel.setText("Logged: ");
    countPanel.add(loggedLabel);

    loggedCount.setEditable(false);
    loggedCount.setColumns(5);
    loggedCount.setText("00000");
    countPanel.add(loggedCount);

    adminLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    adminLabel.setText("Adm: ");
    countPanel.add(adminLabel);

    adminCount.setEditable(false);
    adminCount.setColumns(5);
    adminCount.setText("00000");
    countPanel.add(adminCount);

    cfgLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    cfgLabel.setText("Cfg: ");
    countPanel.add(cfgLabel);

    cfgCount.setEditable(false);
    cfgCount.setColumns(5);
    cfgCount.setText("00000");
    countPanel.add(cfgCount);

    statusLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    statusLabel.setText("Sta: ");
    countPanel.add(statusLabel);

    statusCount.setEditable(false);
    statusCount.setColumns(5);
    statusCount.setText("00000");
    countPanel.add(statusCount);

    eventLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    eventLabel.setText("Evt: ");
    countPanel.add(eventLabel);

    eventCount.setEditable(false);
    eventCount.setColumns(5);
    eventCount.setText("00000");
    countPanel.add(eventCount);

    sensorCntLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    sensorCntLabel.setText("Sensor: ");
    countPanel.add(sensorCntLabel);

    sensorCount.setEditable(false);
    sensorCount.setColumns(5);
    sensorCount.setText("00000");
    countPanel.add(sensorCount);

    dataPanel.add(countPanel, java.awt.BorderLayout.NORTH);

    mainPanel.add(dataPanel, java.awt.BorderLayout.CENTER);

    getContentPane().add(mainPanel, java.awt.BorderLayout.CENTER);

    editPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Edit", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
    editPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    fileLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    fileLabel.setText("Output File: ");
    editPanel.add(fileLabel);

    selectFileButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    selectFileButton.setText("File");
    selectFileButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        selectFileButtonActionPerformed(evt);
      }
    });
    editPanel.add(selectFileButton);

    fileText.setEditable(false);
    fileText.setColumns(60);
    fileText.setToolTipText("Selected output file");
    editPanel.add(fileText);

    wrapCheckBox.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    wrapCheckBox.setText("Wrap Lines");
    wrapCheckBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        wrapCheckBoxActionPerformed(evt);
      }
    });
    editPanel.add(wrapCheckBox);

    connBtn.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    connBtn.setText("Connect");
    connBtn.setEnabled(false);
    connBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        connBtnActionPerformed(evt);
      }
    });
    editPanel.add(connBtn);

    connStatusLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    connStatusLabel.setText("Conn: ");
    editPanel.add(connStatusLabel);

    connActValue.setEditable(false);
    connActValue.setText("Disconnected");
    editPanel.add(connActValue);

    getContentPane().add(editPanel, java.awt.BorderLayout.NORTH);

    mainStatusPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    mainStatusPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    mainStatusLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    mainStatusLabel.setText("Status: ");
    mainStatusPanel.add(mainStatusLabel);

    mainStatusText.setEditable(false);
    mainStatusText.setColumns(100);
    mainStatusText.setText("Starting up");
    mainStatusPanel.add(mainStatusText);

    getContentPane().add(mainStatusPanel, java.awt.BorderLayout.SOUTH);

    fileMenu.setText("File");

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        exitMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(exitMenuItem);

    recorderMenuBar.add(fileMenu);

    editMenu.setText("Edit");

    editMenuItem.setText("Select Output...");
    editMenuItem.setToolTipText("Change output file.");
    editMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        editMenuItemActionPerformed(evt);
      }
    });
    editMenu.add(editMenuItem);

    editConnMenuItem.setText("Edit ISA Connection...");
    editConnMenuItem.setToolTipText("Edit the ISA connection parameters.");
    editConnMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        editConnMenuItemActionPerformed(evt);
      }
    });
    editMenu.add(editConnMenuItem);

    editSelectMenuItem.setText("Edit ISA Query...");
    editSelectMenuItem.setToolTipText("Edit the message query.");
    editSelectMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        editSelectMenuItemActionPerformed(evt);
      }
    });
    editMenu.add(editSelectMenuItem);

    recorderMenuBar.add(editMenu);

    helpMenuItem.setText("Help");

    aboutBoxMenuItem.setText("About...");
    aboutBoxMenuItem.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        aboutBoxMenuItemActionPerformed(evt);
      }
    });
    helpMenuItem.add(aboutBoxMenuItem);

    recorderMenuBar.add(helpMenuItem);

    setJMenuBar(recorderMenuBar);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exitMenuItemActionPerformed
  {//GEN-HEADEREND:event_exitMenuItemActionPerformed
    saveAppState();
    if ( outFileJCAD != null )
    {
      try
      {
        outFileJCAD.write("# Output capture terminated.\n");
        outFileJCAD.flush();
        outFileJCAD.close();
        outFileHapsite.write("# Output capture terminated.\n");
        outFileHapsite.flush();
        outFileHapsite.close();
        outFile.write("# Output capture terminated.\n");
        outFile.flush();
        outFile.close();
      } catch ( IOException ex )
      {
        MY_LOGGER.error("Exception flushing or closing the output file." , ex);
      }
    }
    JHRMRecorder.getMY_FRAME().dispose();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void selectFileButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_selectFileButtonActionPerformed
  {//GEN-HEADEREND:event_selectFileButtonActionPerformed
    FileFilter filter = new FileNameExtensionFilter("Captured Messages" , "txt");
    File output = selectFile(filter , "Select Output File" , lastOutputDir , lastOutputFile , false , true);
    if ( output != null )
    {
      String outputJCAD = null,
             outputHapsite = null,
             outputMultiRAE = null;
      
      if ( KnownSensorType.JCAD.isEnabled() )
      {
        outputJCAD = output.getParentFile().getAbsolutePath() + File.separator + "JCAD_" + output.getName();
        outJCAD = new File(outputJCAD);
      }
      if ( KnownSensorType.HAPSITE.isEnabled() )
      {
        outputHapsite = output.getParentFile().getAbsolutePath() + File.separator + "Hapsite_" + output.getName();
        outHapsite = new File(outputHapsite);
      }
      if ( KnownSensorType.MULTIRAE.isEnabled() )
      {
        outputMultiRAE = output.getParentFile().getAbsolutePath() + File.separator + "MultiRAE_" + output.getName();
        outMultiRAE = new File(outputMultiRAE);
      }
      fileText.setText(output.getAbsolutePath());
      lastOutputDir = output.getParentFile();
      lastOutputFile = output.getAbsoluteFile();
      selectFileButton.setEnabled(false);
      try
      {
        outFile = new BufferedWriter(new FileWriter(output , false));
        writeToOutput("# Output capture initialized." , outFile);

        if ( outJCAD != null )
        {
          if ( this.appendFlag )
          {
            outFileJCAD = new BufferedWriter(new FileWriter(outJCAD , true));
          } 
          else
          {
            outFileJCAD = new BufferedWriter(new FileWriter(outJCAD , false));
          }
          writeToOutput("# Output capture initialized." , outFileJCAD);
        }
        if ( outHapsite != null )
        {
          if ( this.appendFlag )
          {
            outFileHapsite = new BufferedWriter(new FileWriter(outHapsite , true));
          } 
          else
          {
            outFileHapsite = new BufferedWriter(new FileWriter(outHapsite , false));
          }
          writeToOutput("# Output capture initialized." , outFileHapsite);
        }
//        if ( outMultiRAE != null )
//        {
//          if ( this.appendFlag )
//          {
//            outFileMultiRAE = new BufferedWriter( new FileWriter( outMultiRAE , true ) );
//          }
//          else
//          {
//            outFileMultiRAE = new BufferedWriter( new FileWriter( outMultiRAE , false ) );
//          }
//          writeToOutput( "# Output capture initialized." , outFileMultiRAE );
//        }
        connBtn.setEnabled(true);
      } 
      catch ( IOException ioEx )
      {
        MY_LOGGER.error("Exception opening output file: " + output.getAbsolutePath() , ioEx);
      }
    }
  }//GEN-LAST:event_selectFileButtonActionPerformed

  private void wrapCheckBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_wrapCheckBoxActionPerformed
  {//GEN-HEADEREND:event_wrapCheckBoxActionPerformed
    if ( wrapCheckBox.isSelected() )
    {
      logTextArea.setLineWrap(true);
      logTextArea.setWrapStyleWord(true);
    } else
    {
      logTextArea.setLineWrap(false);
      logTextArea.setWrapStyleWord(false);
    }
  }//GEN-LAST:event_wrapCheckBoxActionPerformed

  private void connBtnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_connBtnActionPerformed
  {//GEN-HEADEREND:event_connBtnActionPerformed
    JButton btn = this.connBtn;
    if ( btn.getText().equalsIgnoreCase("Connect") )
    {
      this.setConnActValue("Connecting...");
      writeToViewingArea("Connecting..");
      if ( JHRMRecorder.createThreadPool() )
      {
        writeToViewingArea("Thread pool created.");
        JHRMRecorder.getCTL_QUEUE().setLOCK_QUEUE_WRITE(false);
        JHRMRecorder.getROUTER_QUEUE().setLOCK_QUEUE_WRITE(false);
        JHRMRecorder.getJCAD_QUEUE().setLOCK_QUEUE_WRITE(false);
        JHRMRecorder.getHAPSITE_QUEUE().setLOCK_QUEUE_WRITE(false);
        JHRMRecorder.getMULTIRAE_QUEUE().setLOCK_QUEUE_WRITE(false);
        JHRMRecorder.getCTL_QUEUE().setLOCK_QUEUE_READ(false);
        JHRMRecorder.getROUTER_QUEUE().setLOCK_QUEUE_READ(false);
        JHRMRecorder.getJCAD_QUEUE().setLOCK_QUEUE_READ(false);
        JHRMRecorder.getHAPSITE_QUEUE().setLOCK_QUEUE_READ(false);
        JHRMRecorder.getMULTIRAE_QUEUE().setLOCK_QUEUE_READ(false);
        routerInterface = new MessageRouter();
        routerThread = JHRMRecorder.getPOOL().submit(routerInterface);
        jcadProcessor = new JCADReportProcessor(outFileJCAD);
        jcadThread = JHRMRecorder.getPOOL().submit(jcadProcessor);
        hapsiteProcessor = new HapsiteReportProcessor(outFileHapsite);
        hapsiteThread = JHRMRecorder.getPOOL().submit(hapsiteProcessor);
//      multiraeInterface = new MultiRAEReportProcessor( outFile );
//      multiraeThread = JHRMRecorder.getPOOL().submit( multiraeProcessor );
        isaInterface = new JHRMRecorderISA(outFile);
        isaThread = JHRMRecorder.getPOOL().submit(isaInterface);
        btn.setText("Disconnect");
      } else
      {
        String msg = "*** Error starting thread pool";
        writeToViewingArea(msg);
        MY_LOGGER.error(msg);
      }
    }
    else
    {
      btn.setText("Connect");
      this.setConnActValue("Disconnecting...");
      writeToViewingArea("Disconnecting");
      writeToStatus("Disconnecting...");
      JHRMRecorder.getCTL_QUEUE().insertFirst(new JHRMRecorderCtlQueueEntry(IPCQueueEventType.IPC_SHUTDOWN));
      JHRMRecorder.getROUTER_QUEUE().insertFirst(new RouterDataQueueEntry(IPCQueueEventType.IPC_SHUTDOWN));
      JHRMRecorder.getJCAD_QUEUE().insertFirst(new JCADDataQueueEntry(IPCQueueEventType.IPC_SHUTDOWN));
      JHRMRecorder.getHAPSITE_QUEUE().insertFirst(new HapsiteDataQueueEntry(IPCQueueEventType.IPC_SHUTDOWN));
//      JHRMRecorder.getMULTIRAE_QUEUE().insertFirst( new MultiRAEDataQueueEntry( IPCQueueEventType.IPC_SHUTDOWN ) );
      try
      {
        JHRMRecorder.getCTL_QUEUE().setLOCK_QUEUE_WRITE(true);
        JHRMRecorder.getROUTER_QUEUE().setLOCK_QUEUE_WRITE(true);
        JHRMRecorder.getJCAD_QUEUE().setLOCK_QUEUE_WRITE(true);
        JHRMRecorder.getHAPSITE_QUEUE().setLOCK_QUEUE_WRITE(true);
//        JHRMRecorder.getMULTIRAE_QUEUE().setLOCK_QUEUE_WRITE( true );
        writeToViewingArea("Queues Locked");
        JHRMRecorder.getPOOL().shutdown();
//        JHRMRecorder.getCTL_QUEUE().flushQueue();
//        JHRMRecorder.getROUTER_QUEUE().flushQueue();
//        JHRMRecorder.getJCAD_QUEUE().flushQueue();
//          JHRMRecorder.getHAPSITE_QUEUE().flushQueue();
//          JHRMRecorder.getMULTIRAE_QUEUE().flushQueue();
//        JHRMRecorder.getPOOL().purge();
        if ( !JHRMRecorder.getPOOL().awaitTermination(10L , TimeUnit.SECONDS) )
        {
          this.writeToViewingArea("Forcing pool termination.");
          JHRMRecorder.getCTL_QUEUE().flushQueue();
          JHRMRecorder.getROUTER_QUEUE().flushQueue();
          JHRMRecorder.getJCAD_QUEUE().flushQueue();
          JHRMRecorder.getHAPSITE_QUEUE().flushQueue();
//          JHRMRecorder.getMULTIRAE_QUEUE().flushQueue();
          writeToViewingArea("Queues Flushed");
          JHRMRecorder.getPOOL().shutdownNow();
          JHRMRecorder.getPOOL().purge();
          if ( !JHRMRecorder.getPOOL().awaitTermination(10L , TimeUnit.SECONDS) )
          {
            MY_LOGGER.error("Pool did not terminate");

            this.writeToViewingArea("Pool did not terminate.");
            if ( !isaThread.isDone() )
            {
              this.writeToViewingArea("Cancelling ISA thread.");
              isaThread.cancel(true);
            }
            if ( !routerThread.isDone() )
            {
              this.writeToViewingArea("Cancelling Router thread.");
              routerThread.cancel(true);
            }
            if ( !jcadThread.isDone() )
            {
              this.writeToViewingArea("Cancelling JCAD thread.");
              jcadThread.cancel(true);
            }
            if ( !hapsiteThread.isDone() )
            {
              this.writeToViewingArea("Cancelling Hapsite thread.");
              hapsiteThread.cancel(true);
            }
//            if ( !multiraeThread.isDone() )
//            {
//              this.writeToViewingArea( "Cancelling MultiRAE thread." );
//              multiraeThread.cancel( true );
//            }
          }
        } else
        {
          this.writeToViewingArea("Pool is terminated.");
        }
        if ( !JHRMRecorder.destroyThreadPool() )
        {
          String msg = "*** Error destroying thread pool.";
          writeToViewingArea(msg);
          MY_LOGGER.error(msg);
        } else
        {
          writeToViewingArea("Thread pool destroyed.");
        }
        writeToViewingArea("Disconnected");
        writeToStatus("Disconnected");
      } 
      catch ( InterruptedException jEx )
      {
        MY_LOGGER.error("Exception cleaning up threads" , jEx);
        this.writeToViewingArea("*** Exception cleaning up threads.");
      }

      if ( outFile != null )
      {
        try
        {
          outFile.write("# Output capture terminated due to disconnect.\n");
          outFile.flush();
          outFile.close();
          if ( outFileJCAD != null )
          {
            outFileJCAD.flush();
            outFileJCAD.close();
          }
          if ( outFileHapsite != null )
          {
            outFileHapsite.flush();
            outFileHapsite.close();
          }
          if ( outFileMultiRAE != null )
          {
            outFileMultiRAE.flush();
            outFileMultiRAE.close();
          }
        }
        catch ( IOException ex )
        {
          MY_LOGGER.error("Exception closing output capture." , ex);
        }
      }
      this.setConnActValue("Disconnected.");
      writeToViewingArea("Disconnected.");
    }
  }//GEN-LAST:event_connBtnActionPerformed

  private void logTextAreaMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_logTextAreaMousePressed
  {//GEN-HEADEREND:event_logTextAreaMousePressed
    JTextArea ta = this.logTextArea;
    if ( evt.isPopupTrigger() )
    {
      ta.requestFocusInWindow();
      this.showRMM(ta , evt.getX() , evt.getY());
    }
  }//GEN-LAST:event_logTextAreaMousePressed

  private void editMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_editMenuItemActionPerformed
  {//GEN-HEADEREND:event_editMenuItemActionPerformed
    selectFileButton.setEnabled(true);
  }//GEN-LAST:event_editMenuItemActionPerformed

  private void logTextAreaMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_logTextAreaMouseReleased
  {//GEN-HEADEREND:event_logTextAreaMouseReleased
    JTextArea ta = this.logTextArea;
    if ( evt.isPopupTrigger() )
    {
      ta.requestFocusInWindow();
      this.showRMM(ta , evt.getX() , evt.getY());
    }
  }//GEN-LAST:event_logTextAreaMouseReleased

  private void aboutBoxMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_aboutBoxMenuItemActionPerformed
  {//GEN-HEADEREND:event_aboutBoxMenuItemActionPerformed
    showAboutBox();
  }//GEN-LAST:event_aboutBoxMenuItemActionPerformed

  private void editConnMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_editConnMenuItemActionPerformed
  {//GEN-HEADEREND:event_editConnMenuItemActionPerformed
    JHRMRecorderEditConnDialog dialog = new JHRMRecorderEditConnDialog(this , true);
    dialog.setVisible(true);
    switch ( dialog.getRetStatus() )
    {
      case JOptionPane.OK_OPTION:
        MY_LOGGER.info("Successfully updated connection properties.");
        break;

      case JOptionPane.CANCEL_OPTION:
        MY_LOGGER.debug("User cancelled connection parameter edit.");
        break;

      case JOptionPane.ERROR:
        JOptionPane.showMessageDialog(this , "Invalid connection information was input.\nPlease try again." ,
          "Invalid Connection Info" , JOptionPane.ERROR_MESSAGE);
        break;

      default:
        break;
    }
  }//GEN-LAST:event_editConnMenuItemActionPerformed

  private void editSelectMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_editSelectMenuItemActionPerformed
  {//GEN-HEADEREND:event_editSelectMenuItemActionPerformed
    JHRMRecorderEditSelectDialog dialog = new JHRMRecorderEditSelectDialog(this , true);
    dialog.setVisible(true);
  }//GEN-LAST:event_editSelectMenuItemActionPerformed

  /**
   * The mouse listener for the popup menu
   */
  class PopupListener extends MouseAdapter
  {

    /**
     * Constructor
     *
     * @param which the text area
     */
    public PopupListener(JTextArea which)
    {

    }

    @Override
    public void mousePressed(MouseEvent e)
    {
      super.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
      if ( SwingUtilities.isRightMouseButton(e) )
      {
        showPopup(e);
      } else
      {
        super.mousePressed(e);
      }
    }

    /**
     * Show the popup menu
     *
     * @param e the mouse event
     */
    private void showPopup(MouseEvent e)
    {
      // Ignore this event
    }
  }

  /**
   * The search popup menu for the viewing area
   */
  class SearchPopupMenu extends JPopupMenu implements java.awt.event.ActionListener
  {

    private static final long serialVersionUID = -7827724436804559149L;
    JTextArea which = null;
    javax.swing.JMenuItem clearItem = null;
    javax.swing.JMenuItem copyItem = null;
    javax.swing.JMenuItem cutItem = null;
    javax.swing.JMenuItem clearAllItem = null;
    javax.swing.JMenuItem selectAllItem = null;
    javax.swing.JLabel popupLabel = null;
    javax.swing.JMenuItem findItem = null;

    /**
     * Construct an instance for a specific text area
     *
     * @param which the text area
     */
    public SearchPopupMenu(JTextArea which)
    {
      super();
      super.setBorderPainted(true);
      this.which = which;
      clearItem = new javax.swing.JMenuItem("Clear");
      copyItem = new javax.swing.JMenuItem("Copy");
      selectAllItem = new javax.swing.JMenuItem("SelectAll");
      findItem = new javax.swing.JMenuItem("Find...");
      popupLabel = new javax.swing.JLabel();
      popupLabel.setFont(new java.awt.Font("Tahoma" , 1 , 14));
      popupLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
      popupLabel.setText("Text Area Menu");
      clearItem.addActionListener(this);
      copyItem.addActionListener(this);
      selectAllItem.addActionListener(this);
      findItem.addActionListener(this);
      super.add(popupLabel);
      super.addSeparator();
      super.add(clearItem);
      super.add(copyItem);
      super.addSeparator();
      super.add(selectAllItem);
      super.add(findItem);
    }

    @Override
    public void show(Component c , int x , int y)
    {
      if ( c instanceof JTextArea )
      {
        JTextArea ta = (JTextArea) c;

        if ( ta.getSelectedText() != null )
        {
          copyItem.setEnabled(true);
        } else
        {
          copyItem.setEnabled(false);
        }
        if ( ( ta.getText() != null ) && ( ta.getText().trim().length() > 0 ) )
        {
          selectAllItem.setEnabled(true);
        } else
        {
          selectAllItem.setEnabled(false);
        }
        super.show(c , x , y);
      }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
      if ( e.getActionCommand().equalsIgnoreCase("Clear") )
      {
        JHRMRecorder.getMY_FRAME().clearViewingArea();
      } else if ( e.getActionCommand().equalsIgnoreCase("SelectAll") )
      {
        try
        {
          if ( this.which.getText() != null )
          {
            this.which.setCaretPosition(0);
            this.which.moveCaretPosition(this.which.getLineEndOffset(this.which.getLineCount() - 1));
          }
        } catch ( BadLocationException ex )
        {
          MY_LOGGER.error("Bad location for select all" , ex);
        }
      } else if ( e.getActionCommand().equalsIgnoreCase("Copy") )
      {
        which.copy();
      } else if ( e.getActionCommand().startsWith("Find") )
      {
        FindTextDialog myDialog = new FindTextDialog(JHRMRecorder.getMY_FRAME() , false , this.which);
        myDialog.setLocationRelativeTo(this);
        myDialog.setVisible(true);
      }
    }
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JMenuItem aboutBoxMenuItem;
  private javax.swing.JTextField adminCount;
  private javax.swing.JLabel adminLabel;
  private javax.swing.JTextField cfgCount;
  private javax.swing.JLabel cfgLabel;
  private javax.swing.JTextField connActValue;
  private javax.swing.JButton connBtn;
  private javax.swing.JLabel connStatusLabel;
  private javax.swing.JPanel countPanel;
  private javax.swing.JPanel dataPanel;
  private javax.swing.JMenuItem editConnMenuItem;
  private javax.swing.JMenu editMenu;
  private javax.swing.JMenuItem editMenuItem;
  private javax.swing.JPanel editPanel;
  private javax.swing.JMenuItem editSelectMenuItem;
  private javax.swing.JTextField eventCount;
  private javax.swing.JLabel eventLabel;
  protected javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JLabel fileLabel;
  private javax.swing.JMenu fileMenu;
  private javax.swing.JTextField fileText;
  private javax.swing.JMenu helpMenuItem;
  private javax.swing.JTextArea logTextArea;
  private javax.swing.JTextField loggedCount;
  private javax.swing.JLabel loggedLabel;
  private javax.swing.JPanel mainPanel;
  private javax.swing.JLabel mainStatusLabel;
  private javax.swing.JPanel mainStatusPanel;
  private javax.swing.JTextField mainStatusText;
  private javax.swing.JMenuBar recorderMenuBar;
  private javax.swing.JTextField recvdCount;
  private javax.swing.JLabel recvdLabel;
  private javax.swing.JButton selectFileButton;
  private javax.swing.JLabel sensorCntLabel;
  private javax.swing.JTextField sensorCount;
  private javax.swing.JTextField statusCount;
  private javax.swing.JLabel statusLabel;
  private javax.swing.JScrollPane textScrollPane;
  private javax.swing.JCheckBox wrapCheckBox;
  // End of variables declaration//GEN-END:variables
}
