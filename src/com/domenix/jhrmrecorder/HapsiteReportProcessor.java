/*
 * HapsiteReportProcessor.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the ISA client functions.
 *
 * Author: T. Swanson
 * Version: V1.0
 */

package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import gov.isa.model.*;
import gov.isa.net.MetaData;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Extract information from a Hapsite related message and write it to the output.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class HapsiteReportProcessor implements Runnable , IPCQueueListenerInterface
{
  
  /**
   * Flag indicating we should shut down.
   */
  private static final AtomicBoolean SHUTDOWN_FLAG = new AtomicBoolean( false );
  
  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = Logger.getLogger( HapsiteReportProcessor.class );
  
  /**
   * Messages to be processed.
   */
  private final LinkedList<HapsiteDataQueueEntry> messagesToProcess = new LinkedList<>();

  /**
   * The depth of the processing list
   */
//  private final AtomicInteger procDepth = new AtomicInteger( 0 );

  /**
   * The input queue.
   */
  private final IPCQueue<HapsiteDataQueueEntry> theQueue;

  /**
   * The output file.
   */
  private final BufferedWriter outFile;

  /**
   * The display frame.
   */
  private final JHRMRecorderFrame theFrame;

  /**
   * The message information.
   */
  private Message theMsg;
  private Header theHdr;
  private MetaData theMeta;
  
  /**
   * The constructor
   *
   * @param outFile the logging output file
   */
  public HapsiteReportProcessor( BufferedWriter outFile )
  {
    theQueue = JHRMRecorder.getHAPSITE_QUEUE();
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    this.outFile = outFile;
    this.theFrame = JHRMRecorder.getMY_FRAME();
    MY_LOGGER.info( "Report processor constructed." );
  }

  /**
   * Set the shutdown flag
   *
   * @param val the value to be set
   */
  public void setShutdown( boolean val )
  {
    HapsiteReportProcessor.setSHUTDOWN_FLAG( val );
  }
  
  /**
   * Queue entry received.
   * 
   * @param evt the queue event
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    HapsiteDataQueueEntry entry = this.theQueue.readFirst( 0L );
    if ( entry != null )
    {
      switch ( evt.getEventType() )
      {
        case IPC_SHUTDOWN:
          JCADReportProcessor.setSHUTDOWN_FLAG( true );
          break;

        case IPC_NEW:
          this.messagesToProcess.add( entry );
          SwingUtilities.invokeLater( () ->
          {
            try
            {
              if ( this.messagesToProcess.size() >= 1 )
              {
                processInputMessage();
              }
            }
            catch ( Exception ex )
            {
              MY_LOGGER.error( "Exception processing input messages." , ex );
            }
          } );
          break;

        default:
          MY_LOGGER.info( "Received unhandled queue entry type " + evt.getEventType().name() );
          break;
      }
    }
  }
  
  /**
   * Run periodically to process messages from the list.  Runs while there are entries to process.
   */
  private void processInputMessage()
  {
    try
    {
      while ( ( this.messagesToProcess.size() > 0 ) && ( !SHUTDOWN_FLAG.get() ) )
      {
        HapsiteDataQueueEntry entry = this.messagesToProcess.pollFirst();
        theMsg = entry.getTheMsg();
        theHdr = entry.getTheHdr();
        theMeta = entry.getTheMeta();
        if ( theMsg != null )
        {
          this.processMessage( theMsg , theHdr , theMeta );
        }
      }
    }
    catch( Exception ex )
    {
      MY_LOGGER.error( "Exception processing message." , ex );
    }
  }
  
  /**
   * Processes the message, writes the record log entries, and returns the status
   * 
   * @param theMsg the ISA message
   * @param theHdr the ISA message header
   * @param theMeta the ISA message metadata
   *
   * @return flag indicating success/failure, ignoring unused messages is success
   */
  private boolean processMessage( Message theMsg , Header theHdr , MetaData theMeta )
  {
    boolean retValue = false;
    HapsiteReportProcessor.KnownInfoList infoList = this.getKnownSensorInfoList();
    String theSensor = theMsg.getSource().uci();
    UTC reportTimeUTC = theMsg.getTime();
    double reportTime = reportTimeUTC.getValue().atomicValue();
    long identifier = theMsg.getIdentifier();
    if ( theMsg instanceof Administrative )
    {
      ArrayList<String> aboutUs = new ArrayList<>();
      ImmutableList<UCI> about = ( (Administrative) theMsg ).getAbout();
      about.stream().
        map( (x) -> x.uci() ).
        forEachOrdered( (work) ->
        {
          infoList.theList.stream().
            filter( (y) -> ( ( work.toUpperCase().contains( "HAPSITE" ) ) && ( y.enabled ) &&
                             ( !work.contains( "Replay" ) ) ) ).
            forEachOrdered( (_item) ->
            {
              aboutUs.add( work );
            } );
        } );
      theFrame.incrementAdminCount();
      retValue = dumpAdminMessage( theMsg , theSensor , identifier , reportTime , aboutUs );
    }
    else if ( theMsg instanceof Event )
    {
      theFrame.incrementEventCount();
      retValue = this.dumpEventMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Config )
    {
      theFrame.incrementConfigCount();
      retValue = this.dumpConfigMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Status )
    {
      theFrame.incrementStatusCount();
      retValue = this.dumpStatusMessage( theMsg , theSensor , identifier , reportTime );
    }
    else
    {
      retValue = true;  // Ignore things we don't care about
    }
    return ( retValue );
  }
  
  /**
   * Process an event message.
   *
   * @param msg        the message
   * @param theSensor  the sensor UCI
   * @param identifier the message identifier
   * @param reportTime the time of the report
   *
   * @return the status of the result
   */
  private boolean dumpEventMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;

    try
    {
      Event event = (Event) msg;
      EventType.Predefined eventType = event.getType().predef();
      String logMsg = "Processing Event " + eventType.name() + " message from: " + theSensor;
      MY_LOGGER.info( logMsg );
      this.updateDisplay( logMsg );
      JSONObject finalObj = new JSONObject();
      JSONObject eventObj = new JSONObject();
      eventObj.put( "source" , theSensor );
      eventObj.put( "identifier" , identifier );
      long priority = 80L;
      if ( event.getPriority().isPresent() )
      {
        priority = event.getPriority().get().atomicValue();
      }
      eventObj.put( "priority" , priority );
      eventObj.put( "time" , reportTime );
      long eventId = event.getEventID().event_id();
      eventObj.put( "event ID" , eventId );
      if ( event.getEventReference().isPresent() )
      {
        JSONObject evtRefObj = new JSONObject();
        String refuci = event.getEventReference().get().getCreator().uci();
        long refevt = event.getEventReference().get().getId().event_id();
        double reftim = event.getEventReference().get().getTime().getValue().secs();
        evtRefObj.put( "creator" , refuci );
        evtRefObj.put( "id" , refevt );
        evtRefObj.put( "time" , reftim );
        eventObj.put( "event reference" , evtRefObj );
      }
      eventObj.put( "type" , eventType.name() );
      switch ( eventType )
      {
        case ALARM:
        case ALERT:
        case DETECTION:
        case MEASUREMENT:
        {
          ImmutableList<NameValuePair> observables = event.getObservables();
          if ( observables.size() > 0 )
          {
            JSONObject obsObj = new JSONObject();
            for ( NameValuePair x : observables )
            {
              if ( x.getName().equalsIgnoreCase( "Chemical Reading" ) )
              {
                JSONObject chemReading = new JSONObject();
                ChemicalReading cReading = (ChemicalReading) x.getValue();
                if ( cReading.getMaterialClass().isPresent() )
                {
                  chemReading.put( "material class" , cReading.getMaterialClass().get().value() );
                }
                else
                {
                  chemReading.put( "material class" , "UNKNOWN" );
                }
                if ( cReading.getMaterialName().isPresent() )
                {
                  chemReading.put( "material name" , cReading.getMaterialName().get() );
                }
                else
                {
                  chemReading.put( "material name" , "None" );
                }
                if ( cReading.getServiceNumber().isPresent() )
                {
                  chemReading.put( "service number" , cReading.getServiceNumber().get() );
                }
                else
                {
                  chemReading.put( "service number" , "None" );
                }
                if ( cReading.getHarmful().isPresent() )
                {
                  chemReading.put( "harmful" , cReading.getHarmful().get() );
                }
                else
                {
                  chemReading.put( "harmful" , true );
                }
                if ( cReading.getDensity().isPresent() )
                {
                  chemReading.put( "density" , cReading.getDensity().get().getValue().kgpm3() );
                }
                else
                {
                  chemReading.put( "density" , 0.0 );
                }
                obsObj.put( "Chemical Reading" , chemReading );
              }
              else if ( x.getName().equalsIgnoreCase( "Position" ) )
              {
                JSONObject posObj = new JSONObject();
                GeographicPosition pos = (GeographicPosition) ( x.getValue() );
                posObj.put( "latitude" , pos.getLatitude().degrees() );
                posObj.put( "longitude" , pos.getLatitude().degrees() );
                if ( pos.getAltitude().isPresent() )
                {
                  posObj.put( "altitude" , pos.getAltitude().get().meters() );
                }
                else
                {
                  posObj.put( "altitude" , 0.0 );
                }
                obsObj.put( "Position" , posObj );
              }
            }
            eventObj.put( "observables" , obsObj );  // end Observables
          }

          ImmutableList<NameValuePair> properties = event.getDetectorProperties();
          if ( !properties.isEmpty() )
          {
            JSONObject detPropObj = new JSONObject();
            for ( NameValuePair x : event.getDetectorProperties() )
            {
              if ( x.getName().equals( "AdditionalMeasurements" ) )
              {
                JSONObject addMeasObj = new JSONObject();
                ImmutableList<NameValuePair> detProp = ( (CustomType) x.getValue() ).getFields();
                if ( ( detProp != null ) && ( !detProp.isEmpty() ) )
                {
                  for ( NameValuePair p : detProp )
                  {
                    if ( p.getName().equalsIgnoreCase( "scanMode" ) )
                    {
                      addMeasObj.put( "scanMode" , ( (String) p.getValue() ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "methodState" ) )
                    {
                      addMeasObj.put( "methodState" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "methodType" ) )
                    {
                      addMeasObj.put( "methodType" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "methodName" ) )
                    {
                      addMeasObj.put( "methodName" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "materialName" ) )
                    {
                      addMeasObj.put( "materialName" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "units" ) )
                    {
                      addMeasObj.put( "units" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "methodState" ) )
                    {
                      addMeasObj.put( "methodState" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "startTime" ) )
                    {
                      addMeasObj.put( "startTime" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "purity" ) )
                    {
                      addMeasObj.put( "purity" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "alarmLevel" ) )
                    {
                      addMeasObj.put( "alarmLevel" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "fit" ) )
                    {
                      addMeasObj.put( "fit" , (String) p.getValue() );
                    }
                    else if ( p.getName().equalsIgnoreCase( "resultID" ) )
                    {
                      addMeasObj.put( "resultID" , (String) p.getValue() );
                    }
                  }
                  detPropObj.put( "AdditionalMeasurements" , addMeasObj );
                }
              }
              else
              {
                detPropObj.put( x.getName() , x.getValue().toString() );
              }
            }
            eventObj.put( "detector properties" , detPropObj );// end detector properties
          }
          finalObj.put( "Event" , eventObj );
          try
          {
            MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
            this.outFile.write( finalObj.toString() );
            this.outFile.newLine();
            theFrame.incrementLoggedCount();
            retValue = true;
          }
          catch ( IOException ioEx )
          {
            MY_LOGGER.error( "Exception writing Event message to the output file" , ioEx );
            this.updateDisplay( "Exception writing Event message to the output file: " + ioEx.getLocalizedMessage() );
            retValue = false;
          }
        }
        
        default:
          MY_LOGGER.info( "Skipped unknown event type: " + eventType.name() );
      }
    }
    catch ( IllegalStateException | JSONException ovrEx )
    {
      MY_LOGGER.error( "Exception writing Event message to the output file" , ovrEx );
      this.updateDisplay( "Exception writing Event message to the output file: " + ovrEx.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }
  
  /**
   * Process and output a Status message
   *
   * @param theMsg     the received message
   * @param theSensor  the UCI of the sensor
   * @param identifier the identifier of the message
   * @param reportTime the time of the report
   *
   * @return success/failure
   */
  private boolean dumpStatusMessage( Message theMsg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Status message for: " + theSensor );
      this.updateDisplay( "Processing Status message for: " + theSensor );
      Status staMsg = (Status) theMsg;
      JSONObject finalObj = new JSONObject();
      JSONObject statusObj = new JSONObject();
      long priority = 80L;
      try
      {
        if ( staMsg.getPriority().isPresent() )
        {
          priority = staMsg.getPriority().get().atomicValue();
        }
        statusObj.put( "source" , theSensor );
        statusObj.put( "identifier" , identifier );
        statusObj.put( "priority" , priority );
        statusObj.put( "time" , reportTime );
        ImmutableList<NameValuePair> properties = staMsg.getProperties();
        if ( ( properties != null ) && ( !properties.isEmpty() ) )
        {
          JSONObject propertiesObj = new JSONObject();
          for ( NameValuePair base : properties )
          {
            if ( base.getName().equalsIgnoreCase( "AdditionalProperties" ) )
            {
              JSONObject addPropObj = new JSONObject();
              ImmutableList<NameValuePair> addProp = ( (CustomType) base.getValue() ).getFields();
              for ( NameValuePair y : addProp )
              {
                if ( y.getName().equalsIgnoreCase( "hostname" ) )
                {
                  addPropObj.put( "hostname" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "swVersion" ) )
                {
                  addPropObj.put( "swVersion" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "gcVersion" ) )
                {
                  addPropObj.put( "gcVersion" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "msVersion" ) )
                {
                  addPropObj.put( "msVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "probeVersion" ) )
                {
                  addPropObj.put( "probeVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "headspaceVersion" ) )
                {
                  addPropObj.put( "headspaceVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "situProbeVersion" ) )
                {
                  addPropObj.put( "situProbeVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "extSoftwareVersion" ) )
                {
                  addPropObj.put( "extSoftwareVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "dscVersion" ) )
                {
                  addPropObj.put( "dscVersion" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "batteryState" ) )
                {
                  addPropObj.put( "batteryState" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "voltage" ) )
                {
                  addPropObj.put( "voltage" , ( (String) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "sysState" ) )
                {
                  addPropObj.put( "sysState" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "charge" ) )
                {
                  addPropObj.put( "charge" , (String) y.getValue() );
                }
              }  // end loop
              propertiesObj.put( "AdditionalProperties" , addPropObj );
            }
            else if ( base.getName().equalsIgnoreCase( "Position" ) )
            {
              JSONObject posObj = new JSONObject();
              GeographicPosition pos = (GeographicPosition) ( base.getValue() );
              posObj.put( "latitude" , Double.toString( pos.getLatitude().degrees() ) );
              posObj.put( "longitude" , Double.toString( pos.getLongitude().degrees() ) );
              if ( pos.getAltitude().isPresent() )
              {
                posObj.put( "altitude" , Double.toString( pos.getAltitude().get().meters() ) );
              }
              propertiesObj.put( "Position" , posObj );
            }
            else if ( base.getName().equalsIgnoreCase( "Manufacturer" ) )
            {
              propertiesObj.put( "Manufacturer" , ( (String) base.getValue() ) );
            }
            else if ( base.getName().equalsIgnoreCase( "Minimum Status Interval" ) )
            {
              propertiesObj.put( "Minimum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Maximum Status Interval" ) )
            {
              propertiesObj.put( "Maximum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Serial Number" ) )
            {
              propertiesObj.put( "Serial Number" , ( (String) base.getValue() ).trim() );
            }
            else if ( base.getName().equalsIgnoreCase( "Model" ) )
            {
              propertiesObj.put( "Model" , ( (String) base.getValue() ).trim() );
            }
            else
            {
              MY_LOGGER.debug( "Found property: " + base.getName() + " => " + base.getValue().toString() );
            }
          }
          statusObj.put( "properties" , propertiesObj );
        }
        retValue = true;
        finalObj.put( "Status" , statusObj );
      }
      catch ( NumberFormatException | JSONException ex )
      {
        MY_LOGGER.error( "Exception creating properties object." , ex );
        retValue = false;
      }
      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Status message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Status message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing received message" , ex );
      updateDisplay( "*** Exception processing received message" + ex.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }
  
  /**
   * Logs the content of a configuration message for a sensor.
   *
   * @param msg        the Configuration message
   * @param theSensor  the sensor UCI
   * @param identifier the message ID
   * @param reportTime the message time
   *
   * @return success/failure
   */
  private boolean dumpConfigMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Config message for: " + theSensor );
      this.updateDisplay( "Processing Config message for: " + theSensor );
      Config config = (Config) msg;
      JSONObject finalObj = new JSONObject();
      try
      {
        JSONObject cfgObj = new JSONObject();
        cfgObj.put( "source" , theSensor );
        cfgObj.put( "identifier" , identifier );
        long priority = 80L;
        if ( config.getPriority().isPresent() )
        {
          priority = config.getPriority().get().atomicValue();
        }
        cfgObj.put( "priority" , priority );
        cfgObj.put( "time" , reportTime );

        if ( config.getCcd().isPresent() )
        {
          JSONObject ccdObj = new JSONObject();
          Optional<CapabilityDeclaration> ccd = config.getCcd();
          ImmutableList<PropertyDeclaration> properties = ccd.get().getProperties();
          if ( properties != null )
          {
            JSONArray propertiesObj = new JSONArray();
            for ( PropertyDeclaration x : properties )
            {
              JSONObject propDecl = new JSONObject();
              propDecl.put( "name" , x.getName() );
              propDecl.put( "description" , x.getDescription() );
              propDecl.put( "type" , ((Type)x.getType()).value() );
              propDecl.put( "mutability" , x.getMutability().value() );
              propDecl.put( "structure" , ((Structure)x.getStructure()) );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                propDecl.put( "range" , rangeDecl );
              }
              if ( x.getThreshold().isPresent() )
              {
                propDecl.put( "threshold" , x.getThreshold().toString() );
              }
              propertiesObj.put( propDecl );
            }
            ccdObj.put( "properties" , propertiesObj );
          }
          ImmutableList<CommandDeclaration> commands = ccd.get().getCommands();
          if ( commands != null )
          {
            JSONArray cmdObj = new JSONArray();
            for ( CommandDeclaration x : commands.asList() )
            {
              JSONObject cmdDecl = new JSONObject();
              cmdDecl.put( "name" , x.getName() );
              ImmutableList<ParameterDeclaration> args = x.getArgs();
              if ( ( args != null ) && ( !args.isEmpty() ) )
              {
                JSONArray params = new JSONArray();
                for ( ParameterDeclaration y : args.asList() )
                {
                  JSONObject paramDecl = new JSONObject();
                  paramDecl.put( "name" , y.getName() );
                  paramDecl.put( "description" , y.getDescription() );
                  paramDecl.put( "type" , ((Type)y.getType()).value() );
                  paramDecl.put( "optional" , y.getOptional() );
                  paramDecl.put( "structure" , ((Structure)y.getStructure()).toString() );
                  if ( y.getRange().isPresent() )
                  {
                    JSONObject rangeDecl = new JSONObject();
                    Optional<RangeDeclaration> rd = y.getRange();

                    JSONArray ranges = new JSONArray();
                    for ( Range r : rd.get().getRanges() )
                    {
                      JSONObject range = this.processRange( r );
                      ranges.put( range );
                    }
                    rangeDecl.put( "ranges" , ranges );
                  }
                  params.put( paramDecl );
                }
                cmdDecl.put( "args" , params );  // end args
              }
              if ( x.getDescription().isPresent() )
              {
                cmdDecl.put( "description" , x.getDescription().get() );
              }
              ImmutableList<ResultDeclaration> reslt = x.getResults();
              if ( ( reslt != null ) && ( !reslt.isEmpty() ) )
              {
                JSONArray results = new JSONArray();
                for ( ResultDeclaration z : reslt.asList() )
                {
                  JSONObject resltDecl = new JSONObject();
                  resltDecl.put( "name" , z.getName() );
                  resltDecl.put( "description" , z.getDescription() );
                  resltDecl.put( "type" , ((Type)z.getType()).value() );
                  resltDecl.put( "structure" , ((Structure)z.getStructure()).toString() );
                  resltDecl.put( "optional" , z.getOptional() );
                  if ( z.getRange().isPresent() )
                  {
                    Optional<Range> rd = z.getRange();

                    JSONObject range = this.processRange( (Range)rd );
                    resltDecl.put( "range" , range );
                  }
                  results.put( resltDecl );
                }
                cmdDecl.put( "Result Declaration" , results );  // results
              }
              cmdObj.put( cmdDecl );
            }
            ccdObj.put( "commands" , commands );  // commands
          }

          ImmutableList<ObservableDeclaration> observables = ccd.get().getObservables();
          if ( ( observables != null ) && ( !observables.isEmpty() ) )
          {
            JSONArray obsObj = new JSONArray();
            for ( ObservableDeclaration x : observables.asList() )
            {
              JSONObject obsDecl = new JSONObject();
              obsDecl.put( "name" , x.getName() );
              obsDecl.put( "description" , x.getDescription() );
              obsDecl.put( "type" , ((Type)x.getType()).value() );
              obsDecl.put( "structure" , ((Structure)x.getStructure()).toString() );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                obsDecl.put( "range" , rangeDecl );
              }
              obsObj.put( obsDecl );  // observables
            }
            ccdObj.put( "observables" , obsObj );
          }

          ImmutableList<CustomTypeDeclaration> types = ccd.get().getTypes();
          if ( ( types != null ) && ( !types.isEmpty() ) )
          {
            JSONArray typesObj = new JSONArray();
            for ( CustomTypeDeclaration x : types )
            {
              JSONArray custom = new JSONArray();
              
              JSONObject oneCustom = new JSONObject();
              oneCustom.put( "name" , x.getName() );
              oneCustom.put( "description" , x.getDescription() );
              if ( !x.getFields().isEmpty() )
              {
                JSONArray fieldsObj = new JSONArray();
                ImmutableList<FieldDeclaration> fields = x.getFields();
                for ( FieldDeclaration y : fields )
                {
                  JSONObject fldDecl = new JSONObject();
                  fldDecl.put( "name" , y.getName() );
                  fldDecl.put( "description" , y.getDescription() );
                  fldDecl.put( "type" , ((Type)y.getType()).value() );
                  fldDecl.put( "structure" , ((Structure)y.getStructure()).toString() );
                  fldDecl.put( "optional" , y.getOptional() );
                  fieldsObj.put( fldDecl );
                }
                oneCustom.put( "fields" , fieldsObj );
              }
              custom.put( oneCustom );
              typesObj.put( custom ); // custom type decl
            }
            ccdObj.put( "types" , typesObj ); // types
          }
          cfgObj.put( "ccd" , ccdObj );  // ccd
        }

        ImmutableList<PropertyState> propStates = config.getPropertyStates();
        if ( ( propStates != null ) && ( !propStates.isEmpty() ) )
        {
          JSONArray propStatesObj = new JSONArray();
          for ( PropertyState x : propStates )
          {
            JSONObject pState = new JSONObject();
            pState.put( "name" , x.getName() );
            pState.put( "ready" , x.getReady() );
            pState.put( "reporting" , x.getReporting() );
            propStatesObj.put( pState );  // prop state
          }
          cfgObj.put( "property states" , propStatesObj );  // property states
        }

        ImmutableList<ObservableState> observables = config.getObservableStates();
        if ( ( observables != null ) && ( !observables.isEmpty() ) )
        {
          JSONArray obsStateObj = new JSONArray();
          for ( ObservableState x : observables )
          {
            JSONObject obsState = new JSONObject();
            obsState.put( "name" , x.getName() );
            obsState.put( "ready" , x.getReady() );
            obsState.put( "reporting" , x.getReporting() );
            obsStateObj.put( obsStateObj );
          }
          cfgObj.put( "observable states" , obsStateObj );  // observables
        }

        ImmutableList<CommandState> cmds = config.getCommandStates();
        if ( ( cmds != null ) && ( !cmds.isEmpty() ) )
        {
          JSONArray cmdStateObj = new JSONArray();
          for ( CommandState x : cmds )
          {
            JSONObject cmdState = new JSONObject();
            cmdState.put( "name" , x.getName() );
            cmdState.put( "ready" , x.getReady() );
            cmdStateObj.put( cmdState );  // command state
          }
          cfgObj.put( "command states" , cmdStateObj );  // command states
        }

        ImmutableList<NameValuePair> extras = config.getExtras();
        if ( ( extras != null ) && ( !extras.isEmpty() ) )
        {
          JSONArray extrasObj = new JSONArray();
          for ( NameValuePair x : extras )
          {
            JSONObject nvPairObj = new JSONObject();
            nvPairObj.put( x.getName() , x.getValue().toString() );
            extrasObj.put( nvPairObj );
          }
          cfgObj.put( "extras" , extrasObj );  // extras
        }
        finalObj.put( "Config" , cfgObj );
        retValue = true;
      }
      catch ( IllegalStateException | JSONException exObj )
      {
        MY_LOGGER.error( "Exception building configuration." , exObj );
        retValue = false;
      }

      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Config message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Config message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Config message." , ex );
    }
    return ( retValue );
  }

  /**
   * Returns a JSON Range object or null if an exception occurs.
   * 
   * @param r the range to process
   * 
   * @return the JSON or null
   */
  private JSONObject processRange( Range r )
  {
    JSONObject retValue = new JSONObject();
    try
    {
      if ( r instanceof BoundedRange )
      {
        JSONObject bRange = new JSONObject();
        bRange.put( "high inclusive" , ( (BoundedRange) r ).getHighInclusive() );
        bRange.put( "low inclusive" , ( (BoundedRange) r ).getLowInclusive() );
        if ( r.getDescription().isPresent() )
        {
          bRange.put( "description" , r.getDescription().get() );
        }
        bRange.put( "high" , ( (BoundedRange) r ).getHigh().toString() );
        bRange.put( "low" , ( (BoundedRange) r ).getLow().toString() );
        retValue.put( "BoundedRange" , bRange );
      }
      else if ( r instanceof DiscreteRange )
      {
        JSONObject dRange = new JSONObject();
        if ( r.getDescription().isPresent() )
        {
          dRange.put( "description" , r.getDescription().get() );
        }
        if ( ( (DiscreteRange) r ).getInclusive().isPresent() )
        {
          dRange.put( "inclusive" , ( (DiscreteRange) r ).getInclusive().get().toString() );
        }
        dRange.put( "value" , ( (DiscreteRange) r ).getValue().toString() );
        retValue.put( "DiscreteRange" , dRange );
      }
    }
    catch ( JSONException ex )
    {
      MY_LOGGER.error( "Exception processing a Range" , ex );
      retValue = null;
    }
    return ( retValue );
  }
  
  /**
   * Writes a received Admin message to the log.
   *
   * @param theMsg     the input Admin message
   * @param theSensor  the originating sensor
   * @param identifier the identifier
   * @param reportTime the report time
   * @param aboutUs    the list of known sensors
   *
   * @return flag indicating success/failure
   */
  private boolean dumpAdminMessage( Message theMsg , String theSensor , long identifier , double reportTime ,
                                    ArrayList<String> aboutUs )
  {
    boolean retValue;
    try
    {
      Administrative admin = (Administrative) theMsg;
      AdminCode code = admin.getCode();
      String codeStr = code.value();
      JSONObject finalObj = new JSONObject();
      JSONObject anObj = new JSONObject();
      //
      // Ignore types we don't process
      //
      if ( ( codeStr.contains( "BANDWIDTH" ) ) ||
           ( codeStr.contains( "FOLLOW" ) ) ||
           ( codeStr.contains( "SUBSCRIPT" ) ) ||
           ( codeStr.contains( "OTHER" ) ) )
      {
        return ( true );
      }

      try
      {
        anObj.put( "source" , theSensor );
        anObj.put( "identifier" , identifier );
        anObj.put( "time" , reportTime );
        if ( !aboutUs.isEmpty() )
        {
          anObj.put( "about" , aboutUs );
        }
        else
        {
          anObj.put( "about" , "[]" );
        }
        anObj.put( "code" , code.value() );
        finalObj.put( "Administrative" , anObj );
      }
      catch ( JSONException ex )
      {
        MY_LOGGER.error( "Exception building Admin message data." , ex );
        retValue = true;
        return ( retValue );
      }

      if ( code.value().equalsIgnoreCase( "DEVICE_ADDED" ) )
      {
        aboutUs.stream().
          map( (x) ->
          {
            updateDisplay( x + " has joined the network." );
            return x;
          } ).
          forEachOrdered( (_item) ->
          {
            theFrame.incrementSensorCount();
          } );
      }
      else if ( code.value().equals( "DEVICE_REMOVED" ) )
      {
        aboutUs.stream().
          map( (x) ->
          {
            updateDisplay( x + " has left the network." );
            return x;
          } ).
          forEachOrdered( (_item) ->
          {
            theFrame.decrementSensorCount();
          } );
      }
      else if ( code.value().equals( "DEVICE_NON_RESPONSIVE" ) )
      {
        aboutUs.forEach( (x) ->
        {
          updateDisplay( x + " is not responding on the network." );
        } );
      }
      else if ( code.value().equals( "DEVICE_REVIVED" ) )
      {
        aboutUs.forEach( (x) ->
        {
          updateDisplay( x + " is again responding on the network." );
        } );
      }
      try
      {
        MY_LOGGER.debug( this.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Admin message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Admin message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Admin message" , ex );
      retValue = false;
    }
    return ( retValue );
  }
  
  /**
   * A simple implementation to pretty-print JSON file.
   *
   * @param unformattedJsonString the string to be formatted
   *
   * @return the pretty version
   */
  private String prettyPrintJSON( String unformattedJsonString )
  {
    StringBuilder prettyJSONBuilder = new StringBuilder();
    int indentLevel = 0;
    boolean inQuote = false;
    for ( char charFromUnformattedJson : unformattedJsonString.toCharArray() )
    {
      switch ( charFromUnformattedJson )
      {
        case '"':
          // switch the quoting status
          inQuote = !inQuote;
          prettyJSONBuilder.append( charFromUnformattedJson );
          break;
        case ' ':
          // For space: ignore the space if it is not being quoted.
          if ( inQuote )
          {
            prettyJSONBuilder.append( charFromUnformattedJson );
          }
          break;
        case '{':
        case '[':
          // Starting a new block: increase the indent level
          prettyJSONBuilder.append( charFromUnformattedJson );
          if ( !inQuote )
          {
            indentLevel++;
          }
          appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          break;
        case '}':
        case ']':
          // Ending a new block; decrese the indent level
          if ( !inQuote )
          {
            indentLevel--;
          }
          appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          prettyJSONBuilder.append( charFromUnformattedJson );
          break;
        case ',':
          // Ending a json item; create a new line after
          prettyJSONBuilder.append( charFromUnformattedJson );
          if ( !inQuote )
          {
            appendIndentedNewLine( indentLevel , prettyJSONBuilder );
          }
          break;
        default:
          prettyJSONBuilder.append( charFromUnformattedJson );
      }
    }
    return prettyJSONBuilder.toString();
  }

  /**
   * Print a new line with indention at the beginning of the new line.
   *
   * @param indentLevel   the current indentation level
   * @param stringBuilder the builder for the output string
   */
  private void appendIndentedNewLine( int indentLevel , StringBuilder stringBuilder )
  {
    stringBuilder.append( "\n" );
    for ( int i = 0; i < indentLevel; i++ )
    {
      // Assuming indention using 2 spaces
      stringBuilder.append( "  " );
    }
  }

  /**
   * Run the thread
   */
  @Override
  public void run()
  {
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    boolean doMore = true;
    try
    {
      while ( ( doMore ) && ( !HapsiteReportProcessor.SHUTDOWN_FLAG.get() ) )
      {
        try
        {
          Thread.sleep( 500L );
        }
        catch ( InterruptedException intEx )
        {
          doMore = false;
          HapsiteReportProcessor.setSHUTDOWN_FLAG( true );
          MY_LOGGER.error( "Hapsite run method interrupted." , intEx );
        }
      }
    }
    catch ( Exception ex )
    {
      MY_LOGGER.error( "Exception waiting for shutdown" , ex );
    }
  }
  
  /**
   * Returns known sensor entry information for this application.
   *
   * @return KnownInfoList of the sensor data
   */
  private KnownInfoList getKnownSensorInfoList()
  {
    KnownInfoList retValue = new KnownInfoList();

    for ( int i = 0; i < ( JHRMRecorder.getKNOWN_TYPES().length ); i++ )
    {
      KnownInfo entry = new KnownInfo( JHRMRecorder.getKNOWN_TYPES()[ i ] , JHRMRecorder.getKNOWN_ENABLED()[ i ] );
      retValue.theList.add( entry );
    }
    return ( retValue );
  }
  
  /**
   * Write a line to the display area.
   *
   * @param msg the message to be written
   */
  private void updateDisplay( String msg )
  {
    theFrame.writeToViewingArea( msg );
  }

  /**
   * Sensor known information
   */
  class KnownInfo
  {

    /**
     * The sensor type name
     */
    String name;
    /**
     * Is the sensor enabled?
     */
    boolean enabled;

    /**
     * Constructor
     *
     * @param name    the sensor type name
     * @param enabled the sensor type enable flag
     */
    public KnownInfo( String name , Boolean enabled )
    {
      this.name = name;
      this.enabled = enabled;
    }
  }

  /**
   * A list of KnowInfo records
   */
  class KnownInfoList
  {

    /**
     * The list of entries
     */
    ArrayList<KnownInfo> theList = new ArrayList<>();

    /**
     * Constructor
     */
    public KnownInfoList()
    {
    }
  }

  /**
   * @return the SHUTDOWN_FLAG
   */
  public static boolean getSHUTDOWN_FLAG()
  {
    return SHUTDOWN_FLAG.get();
  }

  /**
   * Set the shutdown flag value
   * 
   * @param val the SHUTDOWN_FLAG value
   */
  public static void setSHUTDOWN_FLAG( boolean val )
  {
    SHUTDOWN_FLAG.set( val );
  }
}
