/*
 * TextAreaUtilities.java - 20/02/22
 *
 * Copyright (c) 2020 Domenix
 *
 * This file contains logic to manipulate text areas.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

//~--- JDK imports ------------------------------------------------------------
import java.awt.Container;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.text.*;
import org.apache.log4j.Logger;

//~--- classes ----------------------------------------------------------------
/**
 * A collection of static methods that provide added functionality for text components (most notably, JTextArea and
 * JTextPane)
 *
 * @version V1.0
 * @author thomas.swanson
 * @see javax.swing.text.Utilities
 */
public class TextAreaUtilities
{

  /**
   * Error/Debug Logger
   */
  private static final Logger MY_LOGGER = Logger.getLogger("TextAreaUtilities");

  /**
   * Attempt to center the line containing the caret at the center of the scroll pane.
   *
   * @param component the text component
   */
  public static void centerLineInScrollPane(JTextComponent component)
  {
    Container container = SwingUtilities.getAncestorOfClass(JViewport.class , component);

    if ( container == null )
    {
      return;
    }

    try
    {
      Rectangle r = component.modelToView(component.getCaretPosition());
      JViewport viewport = (JViewport) container;
      int extentHeight = viewport.getExtentSize().height;
      int viewHeight = viewport.getViewSize().height;
      int y = Math.max(0 , r.y - ( ( extentHeight - r.height ) / 2 ));

      y = Math.min(y , viewHeight - extentHeight);
      viewport.setViewPosition(new Point(0 , y));
    }
    catch ( BadLocationException ble )
    {
      MY_LOGGER.error( "Exception centering line in scroll pane" , ble );
    }
  }

  //~--- get methods ----------------------------------------------------------
  /**
   * Return the column number at the Caret position.
   *
   * The column returned will only make sense when using a monospaced font.
   *
   * @param component the text component
   *
   * @return the caret location
   */
  public static int getColumnAtCaret(JTextComponent component)
  {
    // Since we assume a monospaced font we can use the width of a single
    // character to represent the width of each character
    FontMetrics fm = component.getFontMetrics(component.getFont());
    int characterWidth = fm.stringWidth("0");
    int column = 0;

    try
    {
      Rectangle r = component.modelToView(component.getCaretPosition());
      int width = r.x - component.getInsets().left;

      column = width / characterWidth;
    }
    catch ( BadLocationException ble )
    {
      MY_LOGGER.error( "Exception getting column number" , ble );
    }

    return column + 1;
  }

  /**
   * Get the text component line number of the caret position
   *
   * @param component the text component
   *
   * @return the line number
   */
  public static int getLineAtCaret(JTextComponent component)
  {
    int caretPosition = component.getCaretPosition();
    Element root = component.getDocument().getDefaultRootElement();

    return root.getElementIndex(caretPosition) + 1;
  }

  /**
   * Return the number of lines of text in the Document
   *
   * @param component the text component
   *
   * @return the number of lines
   */
  public static int getLines(JTextComponent component)
  {
    Element root = component.getDocument().getDefaultRootElement();

    return root.getElementCount();
  }

  //~--- methods --------------------------------------------------------------
  /**
   * Position the caret at the start of a line.
   *
   * @param component the text component
   * @param line      the line number to set
   */
  public static void gotoStartOfLine(JTextComponent component , int line)
  {
    Element root = component.getDocument().getDefaultRootElement();

    line = Math.max(line , 1);
    line = Math.min(line , root.getElementCount());

    int startOfLineOffset = root.getElement(line - 1).getStartOffset();

    component.setCaretPosition(startOfLineOffset);
  }

  /**
   * Position the caret on the first word of a line.
   *
   * @param component the component
   * @param line      the line number to set
   */
  public static void gotoFirstWordOnLine(final JTextComponent component , int line)
  {
    gotoStartOfLine(component , line);

    // The following will position the caret at the start of the first word
    try
    {
      int position = component.getCaretPosition();
      String first = component.getDocument().getText(position , 1);

      if ( Character.isWhitespace(first.charAt(0)) )
      {
        component.setCaretPosition(Utilities.getNextWord(component , position));
      }
    }
    catch ( Exception e )
    {
      MY_LOGGER.error( "Exception going to first word in line" , e );
    }
  }

  //~--- get methods ----------------------------------------------------------
  /**
   * Return the number of lines of text, including wrapped lines.
   *
   * @param component the text area
   *
   * @return the number of lines including those wrapped
   */
  public static int getWrappedLines(JTextArea component)
  {
    View view = component.getUI().getRootView(component).getView(0);
    int preferredHeight = (int) view.getPreferredSpan(View.Y_AXIS);
    int lineHeight = component.getFontMetrics(component.getFont()).getHeight();

    return preferredHeight / lineHeight;
  }

  /**
   * Return the number of lines of text, including wrapped lines.
   *
   * @param component the text component
   *
   * @return the number of lines including wrapped lines
   */
  public static int getWrappedLines(JTextComponent component)
  {
    int lines = 0;
    View view = component.getUI().getRootView(component).getView(0);
    int paragraphs = view.getViewCount();

    for ( int i = 0; i < paragraphs; i++ )
    {
      lines += view.getView(i).getViewCount();
    }

    return lines;
  }
}
