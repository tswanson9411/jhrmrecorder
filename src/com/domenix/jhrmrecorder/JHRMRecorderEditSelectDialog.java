/*
 * JHRMRecorderEditSelectDialog.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains a GUI for editing connection properties.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.google.common.collect.ImmutableSet;
import gov.isa.model.DataQuery;
import gov.isa.spi.*;
import java.awt.Color;
import java.awt.Dimension;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 * This dialog allows a user to modify which messages are received and processed.
 * 
 * @author thomas.swanson
 * @version 1.0
 */
public class JHRMRecorderEditSelectDialog extends javax.swing.JDialog
{
  /** Error/Debug Logger */
  Logger MY_LOGGER = Logger.getLogger( JHRMRecorderEditSelectDialog.class );
  
  /** Return status of operations */
  private int retStatus = JOptionPane.ERROR;
  
  /** Test query successful */
  private boolean successfulTest = false;
  /** Selection flags */
  private boolean allSel = false;
  private boolean admSel = false;
  private boolean cfgSel = false;
  private boolean evtSel = false;
  private boolean staSel = false;
  private boolean jcdSel = KnownSensorType.JCAD.isEnabled();
  private boolean hapSel = KnownSensorType.HAPSITE.isEnabled();;
  private boolean raeSel = KnownSensorType.MULTIRAE.isEnabled();;
  private boolean loopBk = false;
  private String  uciSel = "";
  
  /**
   * Creates new form JHRMRecorderEditSelectDialog
   * 
   * @param parent the parent frame
   * @param modal whether or not this is modal
   */
  public JHRMRecorderEditSelectDialog( java.awt.Frame parent , boolean modal )
  {
    super( parent , modal );
    initComponents();
    int myX = this.getX();
    int myY = this.getY();
    int myH = this.getHeight();
    int myW = this.getWidth();
    if ( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.x" ) != null )
    {
      myX = Integer.parseInt( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.x" ) );
    }
    if ( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.y" ) != null )
    {
      myY = Integer.parseInt( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.y" ) );
    }
    if ( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.h" ) != null )
    {
      myH = Integer.parseInt( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.h" ) );
    }
    if ( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.w" ) != null )
    {
      myW = Integer.parseInt( JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.eselect.w" ) );
    }

    this.setSize( new Dimension( myW , myH ) );
    this.setLocation( myX , myY );
    this.validate();
    String query = JHRMRecorder.getCLIENT_PROPERTIES().getProperty( "jhrm.recorder.query" ,
                                                                    "select admin, config, status, event" );
    parseQuery( query );
  }

  /**
   * Parses the current query to set display parameters.
   *
   * @param qry the current query string
   */
  private void parseQuery( String qry )
  {
    String[] parts = qry.split( " " );
    for ( String x : parts )
    {
      if ( x.contains( "select" ) )
      {
      }
      else if ( x.contains( "all" ) )
      {
        allSel = true;
        admSel = true;
        cfgSel = true;
        evtSel = true;
        staSel = true;
      }
      else if ( x.contains( "admin" ) )
      {
        admSel = true;
      }
      else if ( x.contains( "config" ) )
      {
        cfgSel = true;
      }
      else if ( x.contains( "event" ) )
      {
        evtSel = true;
      }
      else if ( x.contains( "status" ) )
      {
        staSel = true;
      }
      else if ( x.contains( "JCAD" ) )
      {
        jcdSel = true;
      }
      else if ( x.contains( "Hapsite" ) )
      {
        hapSel = true;
      }
      else if ( x.contains( "RAE" ) )
      {
        raeSel = true;
      }
      else if ( x.contains( "UCI" ) )
      {
        uciSel = x;
      }
      else if ( x.contains( "loopback" ) )
      {
        loopBk = true;
      }
    }
    this.qryText.setText( qry );
    this.adminChkBox.setSelected( admSel );
    this.cfgChkBox.setSelected( cfgSel );
    this.statusChkBox.setSelected( staSel );
    this.evtChkBox.setSelected( evtSel );
    this.jcadChkBox.setSelected( jcdSel );
    this.hapsiteChkBox.setSelected( hapSel );
    this.multiraeChkBox.setSelected( raeSel );
    this.loopChkBox.setSelected( loopBk );
    if ( !uciSel.isEmpty() )
    {
      String[] uciParts = uciSel.split( "\\{" );
      if ( uciParts.length > 1 )
      {
        String aUci = uciParts[ 1 ];
        this.uciText.setText( aUci );
      }
    }
  }
  
  /**
   * Construct a query string using the select items.
   * 
   * @return the query string
   */
  private String buildQuery()
  {
    StringBuilder query = new StringBuilder( "select " );
    admSel = this.adminChkBox.isSelected();
    cfgSel = this.cfgChkBox.isSelected();
    evtSel = this.evtChkBox.isSelected();
    staSel = this.statusChkBox.isSelected();
    jcdSel = this.jcadChkBox.isSelected();
    hapSel = this.hapsiteChkBox.isSelected();
    raeSel = this.multiraeChkBox.isSelected();
    loopBk = this.loopChkBox.isSelected();
    uciSel = this.uciText.getText().trim();
    boolean first = true;
    
    if ( allChkBox.isSelected() )
    {
      query.append( "all " );
    }
    else
    {
      if ( admSel )
      {
        if ( first )
        {
          first = false;
          query.append( "admin" );
        }
        else
        {
          query.append( ", admin" );
        }
      }
      if ( cfgSel )
      {
        if ( first )
        {
          first = false;
          query.append( "config" );
        }
        else
        {
          query.append( ", config" );
        }
      }
      if ( evtSel )
      {
        if ( first )
        {
          first = false;
          query.append( "event" );
        }
        else
        {
          query.append( ", event" );
        }
      }
      if ( staSel )
      {
        if ( first )
        {
          query.append( "status" );
        }
        else
        {
          query.append( ", status" );
        }
      }
    }
//    if ( jcdSel || hapSel || raeSel )
//    {
//      int countOfSelected = countSelectedSensors();
//      if ( countOfSelected > 1 )
//      {
//        query.append( " matching contains( list: [" );
//        first = true;
//        if ( jcadChkBox.isSelected() )
//        {
//          first = false;
//          query.append( uciSel )
//        }
//        if ( hapsiteChkBox.isSelected() )
//        {
//          retValue++;
//        }
//        if ( multiraeChkBox.isSelected() )
//        {
//          retValue++;
//        }
//      }
//    }
    if ( !uciSel.isEmpty() )
    {
      String[] ucis = uciSel.split( "," );
      if ( ucis.length == 1 )
      {
        query.append( " matching contains( list:[ UCI{" ).append( '"' ).append( uciSel ).append( '"' )
          .append( "}],target: component() )" );
      }
      else if ( ucis.length > 0 )
      {
        first = true;
        query.append( " matching contains( list:[ " );
        for ( String x : ucis )
        {
          if ( first )
          {
            query.append( "UCI{" ).append( '"' ).append( x ).append( '"' ).append( "}" );
            first = false;
          }
          else
          {
            query.append( ", UCI{" ).append( '"' ).append( x ).append( '"' ).append( "}" );
          }
        }
        query.append( "], target:component() )" );
      }
    }
    if ( loopBk )
    {
      query.append( " with loopback" );
    }
    qryText.setForeground( Color.red );
    qryText.setText( query.toString() );
    return( query.toString() );
  }
  
  /**
   * Return a count of the number of selected sensor types.
   * 
   * @return the count
   */
  private int countSelectedSensors()
  {
    int retValue = 0;
    if ( jcadChkBox.isSelected() )
    {
      retValue++;
    }
    if ( hapsiteChkBox.isSelected() )
    {
      retValue++;
    }
    if ( multiraeChkBox.isSelected() )
    {
      retValue++;
    }
    
    return( retValue );
  }
  
  /**
   * Close the dialog
   */
  private void close()
  {
    int myX = this.getX();
    int myY = this.getY();
    int myH = this.getHeight();
    int myW = this.getWidth();
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty( "jhrm.recorder.eselect.x" , Integer.toString( myX ) );
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty( "jhrm.recorder.eselect.y" , Integer.toString( myY ) );
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty( "jhrm.recorder.eselect.h" , Integer.toString( myH ) );
    JHRMRecorder.getCLIENT_PROPERTIES().setProperty( "jhrm.recorder.eselect.w" , Integer.toString( myW ) );
    this.dispose();
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
   * content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents()
  {

    btnPanel = new javax.swing.JPanel();
    okButton = new javax.swing.JButton();
    cancelBtn = new javax.swing.JButton();
    qryPanel = new javax.swing.JPanel();
    txtPanel = new javax.swing.JPanel();
    qryLabel = new javax.swing.JLabel();
    qryText = new javax.swing.JTextField();
    qryBtnPanel = new javax.swing.JPanel();
    testQryBtn = new javax.swing.JButton();
    clrQryBtn = new javax.swing.JButton();
    selectPanel = new javax.swing.JPanel();
    msgPanel = new javax.swing.JPanel();
    allChkBox = new javax.swing.JCheckBox();
    adminChkBox = new javax.swing.JCheckBox();
    cfgChkBox = new javax.swing.JCheckBox();
    evtChkBox = new javax.swing.JCheckBox();
    statusChkBox = new javax.swing.JCheckBox();
    sensorPanel = new javax.swing.JPanel();
    jcadChkBox = new javax.swing.JCheckBox();
    hapsiteChkBox = new javax.swing.JCheckBox();
    multiraeChkBox = new javax.swing.JCheckBox();
    selCtlPanel = new javax.swing.JPanel();
    loopChkBox = new javax.swing.JCheckBox();
    uciLabel = new javax.swing.JLabel();
    uciText = new javax.swing.JTextField();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Edit ISA Selections");

    btnPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    okButton.setText("OK");
    okButton.setEnabled(false);
    okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        okButtonActionPerformed(evt);
      }
    });
    btnPanel.add(okButton);

    cancelBtn.setText("Cancel");
    cancelBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        cancelBtnActionPerformed(evt);
      }
    });
    btnPanel.add(cancelBtn);

    getContentPane().add(btnPanel, java.awt.BorderLayout.SOUTH);

    qryPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    qryPanel.setLayout(new java.awt.GridLayout(2, 1));

    txtPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    qryLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    qryLabel.setText("Query: ");
    txtPanel.add(qryLabel);

    qryText.setColumns(60);
    txtPanel.add(qryText);

    qryPanel.add(txtPanel);

    testQryBtn.setText("Test  Query");
    testQryBtn.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        testQryBtnActionPerformed(evt);
      }
    });
    qryBtnPanel.add(testQryBtn);

    clrQryBtn.setText("Clear Query");
    qryBtnPanel.add(clrQryBtn);

    qryPanel.add(qryBtnPanel);

    getContentPane().add(qryPanel, java.awt.BorderLayout.NORTH);

    selectPanel.setLayout(new java.awt.BorderLayout());

    msgPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Msg Types", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

    allChkBox.setText("All");
    allChkBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        allChkBoxActionPerformed(evt);
      }
    });
    msgPanel.add(allChkBox);

    adminChkBox.setText("Admin");
    msgPanel.add(adminChkBox);

    cfgChkBox.setText("Config");
    msgPanel.add(cfgChkBox);

    evtChkBox.setText("Event");
    msgPanel.add(evtChkBox);

    statusChkBox.setText("Status");
    msgPanel.add(statusChkBox);

    selectPanel.add(msgPanel, java.awt.BorderLayout.NORTH);

    sensorPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sensor Types", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

    jcadChkBox.setText("JCAD");
    jcadChkBox.setEnabled(false);
    sensorPanel.add(jcadChkBox);

    hapsiteChkBox.setText("Hapsite");
    hapsiteChkBox.setEnabled(false);
    sensorPanel.add(hapsiteChkBox);

    multiraeChkBox.setText("MultiRAE");
    multiraeChkBox.setEnabled(false);
    sensorPanel.add(multiraeChkBox);

    selectPanel.add(sensorPanel, java.awt.BorderLayout.CENTER);

    selCtlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Select Control", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N

    loopChkBox.setText("Loopback");
    selCtlPanel.add(loopChkBox);

    uciLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
    uciLabel.setText("UCI: ");
    selCtlPanel.add(uciLabel);

    uciText.setColumns(20);
    uciText.setToolTipText("Enter a single UCI or a comma seperated list.");
    selCtlPanel.add(uciText);

    selectPanel.add(selCtlPanel, java.awt.BorderLayout.SOUTH);

    getContentPane().add(selectPanel, java.awt.BorderLayout.CENTER);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void allChkBoxActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_allChkBoxActionPerformed
  {//GEN-HEADEREND:event_allChkBoxActionPerformed
    if ( allChkBox.isSelected() )
    {
      adminChkBox.setSelected( true );
      cfgChkBox.setSelected( true );
      evtChkBox.setSelected( true );
      statusChkBox.setSelected( true );
    }
    else
    {
      adminChkBox.setSelected( false );
      cfgChkBox.setSelected( false );
      evtChkBox.setSelected( false );
      statusChkBox.setSelected( false );
    }
  }//GEN-LAST:event_allChkBoxActionPerformed

  private void testQryBtnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_testQryBtnActionPerformed
  {//GEN-HEADEREND:event_testQryBtnActionPerformed
    successfulTest = false;
    okButton.setEnabled( false );
    String query = buildQuery();
    //Get a component factory manager
    ComponentFactoryManager manager = new ComponentFactoryManager();

    // Get the default isa api provider from the component factory manager
    ComponentFactoryProvider provider = manager.getDefaultProvider();
    try
    {
      // create a Component factory
      final ComponentFactory factory = provider.initialize(
        ImmutableSet.<ComponentFactoryOption<?>>of(
          StandardComponentFactoryOptions.InitialMinimumStatusInterval.create(
            30 , TimeUnit.SECONDS
          ) ,
          StandardComponentFactoryOptions.InitialMaximumStatusInterval.create(
            60 , TimeUnit.SECONDS
          )
        ) );
      // create a data query manager
      final DataQueryParserManager dqpMgr = new DataQueryParserManager();
      // get the default provider
      final DataQueryParserProvider dqpProvider = dqpMgr.getDefaultProvider();
      // create a parser by passing an appropriate model factory
      final DataQueryParser parser = dqpProvider.createDataQueryParser( factory.getModelFactory() );
      // parse queries from strings (or Readers).
      final DataQuery dataQuery = parser.parseLanguage( query );
      successfulTest = true;
      qryText.setText( query );
      qryText.setForeground( Color.black );
      okButton.setEnabled( true );
    }
    catch( DataQueryParserException e )
    {
      MY_LOGGER.error( "Invalid query parsed; " + query , e );
      String errText = "Invalid query at: " + 
                       ((e.getStartIndex().isPresent())?e.getStartIndex():"0") + " thru " + 
                       ((e.getStopIndex().isPresent())?e.getStopIndex():query.length()-1);
      JOptionPane.showMessageDialog( this, errText , "Query Parsing Error" , JOptionPane.ERROR_MESSAGE );
      qryText.setForeground( Color.red );
    }
    catch ( ComponentFactoryException | IllegalStateException e )
    {
      // detect line and column information in the event of a parsing error.
      MY_LOGGER.error( "Exception testing query: " + query , e );
      JOptionPane.showMessageDialog( this, "Exception testing query: " + query , "Query test Error" , JOptionPane.ERROR_MESSAGE );
    }
  }//GEN-LAST:event_testQryBtnActionPerformed

  private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cancelBtnActionPerformed
  {//GEN-HEADEREND:event_cancelBtnActionPerformed
    retStatus = JOptionPane.CANCEL_OPTION;
    this.setVisible( false );
    close();
  }//GEN-LAST:event_cancelBtnActionPerformed

  private void okButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_okButtonActionPerformed
  {//GEN-HEADEREND:event_okButtonActionPerformed
    if ( this.successfulTest )
    {
      JHRMRecorder.getCLIENT_PROPERTIES().setProperty( "jhrm.recorder.query" , qryText.getText().trim() );
    }
    retStatus = JOptionPane.OK_OPTION;
    this.setVisible( false );
    close();
  }//GEN-LAST:event_okButtonActionPerformed

//  /**
//   * @param args the command line arguments
//   */
//  public static void main( String args[] )
//  {
//    /*
//     * Set the Nimbus look and feel
//     */
//    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//    /*
//     * If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel. For details see
//     * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
//     */
//    try
//    {
//      for ( javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels() )
//      {
//        if ( "Nimbus".equals( info.getName() ) )
//        {
//          javax.swing.UIManager.setLookAndFeel( info.getClassName() );
//          break;
//        }
//      }
//    }
//    catch ( ClassNotFoundException ex )
//    {
//      java.util.logging.Logger.getLogger( JHRMRecorderEditSelectDialog.class.getName() ).log(
//        java.util.logging.Level.SEVERE , null , ex );
//    }
//    catch ( InstantiationException ex )
//    {
//      java.util.logging.Logger.getLogger( JHRMRecorderEditSelectDialog.class.getName() ).log(
//        java.util.logging.Level.SEVERE , null , ex );
//    }
//    catch ( IllegalAccessException ex )
//    {
//      java.util.logging.Logger.getLogger( JHRMRecorderEditSelectDialog.class.getName() ).log(
//        java.util.logging.Level.SEVERE , null , ex );
//    }
//    catch ( javax.swing.UnsupportedLookAndFeelException ex )
//    {
//      java.util.logging.Logger.getLogger( JHRMRecorderEditSelectDialog.class.getName() ).log(
//        java.util.logging.Level.SEVERE , null , ex );
//    }
//    //</editor-fold>
//
//    /*
//     * Create and display the dialog
//     */
//    java.awt.EventQueue.invokeLater( new Runnable()
//    {
//      public void run()
//      {
//        JHRMRecorderEditSelectDialog dialog = new JHRMRecorderEditSelectDialog( new javax.swing.JFrame() , true );
//        dialog.addWindowListener( new java.awt.event.WindowAdapter()
//        {
//          @Override
//          public void windowClosing( java.awt.event.WindowEvent e )
//          {
//            System.exit( 0 );
//          }
//        } );
//        dialog.setVisible( true );
//      }
//    } );
//  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JCheckBox adminChkBox;
  private javax.swing.JCheckBox allChkBox;
  private javax.swing.JPanel btnPanel;
  private javax.swing.JButton cancelBtn;
  private javax.swing.JCheckBox cfgChkBox;
  private javax.swing.JButton clrQryBtn;
  private javax.swing.JCheckBox evtChkBox;
  private javax.swing.JCheckBox hapsiteChkBox;
  private javax.swing.JCheckBox jcadChkBox;
  private javax.swing.JCheckBox loopChkBox;
  private javax.swing.JPanel msgPanel;
  private javax.swing.JCheckBox multiraeChkBox;
  private javax.swing.JButton okButton;
  private javax.swing.JPanel qryBtnPanel;
  private javax.swing.JLabel qryLabel;
  private javax.swing.JPanel qryPanel;
  private javax.swing.JTextField qryText;
  private javax.swing.JPanel selCtlPanel;
  private javax.swing.JPanel selectPanel;
  private javax.swing.JPanel sensorPanel;
  private javax.swing.JCheckBox statusChkBox;
  private javax.swing.JButton testQryBtn;
  private javax.swing.JPanel txtPanel;
  private javax.swing.JLabel uciLabel;
  private javax.swing.JTextField uciText;
  // End of variables declaration//GEN-END:variables

  /**
   * @return the retStatus
   */
  public int getRetStatus()
  {
    return retStatus;
  }
}
