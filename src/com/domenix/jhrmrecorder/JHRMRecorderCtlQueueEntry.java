/*
 * JHRMRecorderCtlQueueEntry.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the definition of a control queue entry.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueueEventType;
import org.apache.log4j.Logger;

/**
 * This defines a control queue entry for the JHRM recorder.  This is used to
 * provide shutdown information.
 * 
 * @author thomas.swanson
 * @version 1.0
 */
public class JHRMRecorderCtlQueueEntry extends com.domenix.utils.IPCQueueInterface
{
  /** Debug/Error logger */
  private final Logger MY_LOGGER = Logger.getLogger(JHRMRecorderCtlQueueEntry.class );
  
  /** Constructor */
  public JHRMRecorderCtlQueueEntry()
  {
    super();
  }
  
  /** Event type constructor.
   * 
   * @param type the event type. 
   */
  public JHRMRecorderCtlQueueEntry( IPCQueueEventType type )
  {
    super( type );
  }
  
  /**
   * Return the current object state as a string.
   * 
   * @return the object state string
   */
  @Override
  public String toString()
  {
    StringBuilder msg = new StringBuilder( "JHRMClientCtlQueueEntry: " );
    msg.append( this.getEventType().name() );
    return ( msg.toString() );
  }
}
