/**
 * Provides the base classes for running the software
 * <p>
 * These are the main, the GUI, and general utility software.
 *
 * @since 1.0
 */
package com.domenix.jhrmrecorder;
