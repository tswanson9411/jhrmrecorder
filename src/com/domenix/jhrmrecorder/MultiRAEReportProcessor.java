/*
 * MultiRAEReportProcessor.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the code for processing received messages from a MultiRAE
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.net.MetaData;
import java.io.BufferedWriter;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.log4j.Logger;

/**
 * Extract information from a MultiRAE related message and write it to the output.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class MultiRAEReportProcessor implements Runnable , IPCQueueListenerInterface
{
  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = Logger.getLogger( MultiRAEReportProcessor.class );

  /**
   * The input queue.
   */
  private final IPCQueue<MultiRAEDataQueueEntry> theQueue;

  /**
   * Flag indicating we should shut down.
   */
  private static final AtomicBoolean SHUTDOWN_FLAG = new AtomicBoolean( false );

  /**
   * The output file.
   */
  private final BufferedWriter outFile;

  /**
   * The display frame.
   */
  private final JHRMRecorderFrame theFrame;

  /**
   * The message information.
   */
  private Message theMsg;
  private Header theHdr;
  private MetaData theMeta;
  
  /**
   * The constructor
   *
   * @param outFile the logging output file
   */
  public MultiRAEReportProcessor( BufferedWriter outFile )
  {
    theQueue = JHRMRecorder.getMULTIRAE_QUEUE();
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    this.outFile = outFile;
    this.theFrame = JHRMRecorder.getMY_FRAME();
    MY_LOGGER.info( "Report processor constructed." );
  }

  /**
   * Set the shutdown flag
   *
   * @param val the value to be set
   */
  public void setShutdown( boolean val )
  {
    MultiRAEReportProcessor.SHUTDOWN_FLAG.set( val );
  }
  
  /**
   * Queue entry received.
   * 
   * @param evt the queue event
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
  }

  /**
   * Run the thread
   */
  @Override
  public void run()
  {
    throw new UnsupportedOperationException( "Not supported yet." ); //To change body of generated methods, choose Tools | Templates.
  }
  
}
