/*
 * UTCConvert.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the logic for routing ISA messages.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import gov.isa.model.UTC;
import java.util.Date;

/**
 * ISA UTC type conversions.
 *
 * @author thomas.swanson
 */
public class UTCConvert
{

  /**
   * Convert an ISA UTC to a java.util.Date
   *
   * @param time the UTC value
   *
   * @return the Date object or null if no time was supplied
   */
  public static Date convertUTCtoDate( UTC time )
  {
    Date retValue = null;

    if ( time != null )
    {
      Double baseDate = ( time.getValue() ).secs() * 1000.0;
      long base2 = baseDate.longValue();
      retValue = new Date( base2 );
    }
    return ( retValue );
  }
}
