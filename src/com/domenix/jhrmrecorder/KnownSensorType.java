/*
 * KnownSensorType.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the definition of known sensor types.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

/**
 * An enumeration of known sensor types that JHRM can process and store.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public enum KnownSensorType
{
  /**
   * Unknown Sensor
   */
  UNKNOWN( "Unknown" , false ),
  /**
   * M4A1 JCAD
   */
  JCAD( "JCAD" , true ),
  /**
   * Hapsite
   */
  HAPSITE( "Hapsite" , true ),
  /**
   * MultiRAE
   */
  MULTIRAE( "MultiRAE" , false );

  /**
   * The display name of this entry
   */
  private final String dispName;
  /**
   * Whether this is enabled or disabled
   */
  private Boolean enabled;

  /**
   * The constructor.
   *
   * @param dispName the display name
   * @param enabled enabled or disabled
   */
  private KnownSensorType( String dispName , boolean enabled )
  {
    this.dispName = dispName;
    this.enabled = enabled;
  }

  /**
   * Get the display name for this type.
   *
   * @return the name
   */
  public String getDisplayName()
  {
    return ( this.dispName );
  }

  /**
   * Return the enabled state for this enumeration.
   *
   * @return the enable value
   */
  public boolean isEnabled()
  {
    return ( this.enabled );
  }

  /**
   * Set the enabled state for this enumeration.
   *
   * @param enabled the enable value
   */
  public void setEnabled( boolean enabled )
  {
    this.enabled = enabled;
  }

  /**
   * Returns the known sensor type for the requested display name.
   *
   * @param disp the display name to look up
   *
   * @return the KnownSensorType or the UNKNOWN type
   */
  public static KnownSensorType lookupByDisplayName( String disp )
  {
    for ( KnownSensorType x : KnownSensorType.values() )
    {
      if ( disp.equalsIgnoreCase( x.dispName ) )
      {
        return ( x );
      }
    }
    return ( KnownSensorType.UNKNOWN );
  }

  /**
   * Return a list of known sensor type names.
   *
   * @return an array of Strings
   */
  public static String[] knownTypeNames()
  {
    int i = 0;
    String[] retValue = new String[ values().length ];
    for ( KnownSensorType x : values() )
    {
      retValue[ i++ ] = ( x.name() );
    }

    return ( retValue );
  }

  /**
   * Return a list of known sensor type enabled flags.
   *
   * @return an array of Boolean
   */
  public static Boolean[] knownTypeEnabled()
  {
    int i = 0;
    Boolean[] retValue = new Boolean[ values().length ];
    for ( KnownSensorType x : values() )
    {
      retValue[ i++ ] = ( x.isEnabled() );
    }

    return ( retValue );
  }
}
