/*
 * HapsiteDataQueueEntry.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the definition of a queue entry with a message payload.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueueEventType;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.net.MetaData;
import org.apache.log4j.Logger;

/**
 * A queue entry with an ISA message payload.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class HapsiteDataQueueEntry extends com.domenix.utils.IPCQueueInterface
{

  /**
   * Debug/Error Logger
   */
  private final Logger MY_LOGGER = Logger.getLogger(HapsiteDataQueueEntry.class );

  /**
   * The message payload.
   */
  private Message theMsg  = null;
  private Header  theHdr  = null;
  private MetaData  theMeta = null;

  /**
   * Default constructor
   */
  public HapsiteDataQueueEntry()
  {
    super();
  }
  
  /**
   * Event type constructor
   * 
   * @param type the event type
   */
  public HapsiteDataQueueEntry( IPCQueueEventType type )
  {
    super( type );
  }
  
  /**
   * Event type and payload constructor
   * 
   * @param type the event type
   * @param msg the ISA message
   * @param hdr the message header
   * @param meta the message meta data
   */
  public HapsiteDataQueueEntry( IPCQueueEventType type , Message msg , Header hdr , MetaData meta )
  {
    super( type );
    this.theMsg = msg;
    this.theHdr = hdr;
    this.theMeta = meta;
  }

  /**
   * @return the theMsg
   */
  public Message getTheMsg()
  {
    return theMsg;
  }

  /**
   * @param theMsg the theMsg to set
   */
  public void setTheMsg( Message theMsg )
  {
    this.theMsg = theMsg;
  }

  /**
   * Return the current object state as a string.
   *
   * @return the object state string
   */
  @Override
  public String toString()
  {
    StringBuilder msg = new StringBuilder( "JHRMRecorderHapsiteDataQueueEntry: " );
    msg.append( this.getEventType().name() );
    msg.append( " Message: " ).append( this.theMsg.toString() );
    return ( msg.toString() );
  }

  /**
   * @return the theHdr
   */
  public Header getTheHdr()
  {
    return theHdr;
  }

  /**
   * @param theHdr the theHdr to set
   */
  public void setTheHdr( Header theHdr )
  {
    this.theHdr = theHdr;
  }

  /**
   * @return the theMeta
   */
  public MetaData getTheMeta()
  {
    return theMeta;
  }

  /**
   * @param theMeta the theMeta to set
   */
  public void setTheMeta( MetaData theMeta )
  {
    this.theMeta = theMeta;
  }
}
